window._CCSettings = {
    platform: "web-desktop",
    groupList: [
        "default"
    ],
    collisionMatrix: [
        [
            true
        ]
    ],
    hasResourcesBundle: true,
    hasStartSceneBundle: false,
    remoteBundles: [],
    subpackages: [],
    launchScene: "db://assets/game.fire",
    orientation: "",
    debug: true,
    jsList: []
};
