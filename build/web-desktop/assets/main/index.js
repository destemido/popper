window.__require = function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var b = o.split("/");
        b = b[b.length - 1];
        if (!t[b]) {
          var a = "function" == typeof __require && __require;
          if (!u && a) return a(b, !0);
          if (i) return i(b, !0);
          throw new Error("Cannot find module '" + o + "'");
        }
        o = b;
      }
      var f = n[o] = {
        exports: {}
      };
      t[o][0].call(f.exports, function(e) {
        var n = t[o][1][e];
        return s(n || e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = "function" == typeof __require && __require;
  for (var o = 0; o < r.length; o++) s(r[o]);
  return s;
}({
  collider: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "21deazBcsNDKZuMcwda1EIx", "collider");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var collider = function(_super) {
      __extends(collider, _super);
      function collider() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.popperID = null;
        _this.colliderDirection = null;
        _this.rocketMove = null;
        _this.rocketTriggered = false;
        _this.collider = null;
        return _this;
      }
      collider.prototype.start = function() {
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
      };
      collider.prototype.init = function(direction, popperID, controller) {
        var _this = this;
        this.colliderDirection = direction;
        this.popperID = popperID;
        this.gameController = controller;
        var popperChild = this.gameController.gridItemManager.grid[this.popperID].gridCurrItem.getChildByName("popper");
        if (0 == direction) {
          var maxDistance_1 = this.node.x + 2 * this.gameController.gridArea.width;
          this.rocketMove = window.setInterval(function() {
            _this.node.x += 3;
            if (_this.node.x > maxDistance_1) {
              window.clearInterval(_this.rocketMove);
              popperChild ? popperChild.getComponent("popper").onPopperTapped() : console.log("Error...");
              _this.destroy();
            }
          }, 10);
        } else if (1 == direction) {
          var maxDistance_2 = this.node.y + 2 * this.gameController.gridArea.height;
          this.rocketMove = window.setInterval(function() {
            _this.node.y += 3;
            if (_this.node.y > maxDistance_2) {
              window.clearInterval(_this.rocketMove);
              popperChild ? popperChild.getComponent("popper").onPopperTapped() : console.log("Error...");
              _this.destroy();
            }
          }, 10);
        } else if (2 == direction) {
          var maxDistance_3 = this.node.x - 2 * this.gameController.gridArea.width;
          this.rocketMove = window.setInterval(function() {
            _this.node.x -= 3;
            if (_this.node.x < maxDistance_3) {
              window.clearInterval(_this.rocketMove);
              popperChild ? popperChild.getComponent("popper").onPopperTapped() : console.log("Error...");
              _this.destroy();
            }
          }, 10);
        } else {
          var maxDistance_4 = this.node.y - 2 * this.gameController.gridArea.height;
          this.rocketMove = window.setInterval(function() {
            _this.node.y -= 3;
            if (_this.node.y < maxDistance_4) {
              window.clearInterval(_this.rocketMove);
              popperChild ? popperChild.getComponent("popper").onPopperTapped() : console.log("Error...");
              _this.destroy();
            }
          }, 10);
        }
      };
      collider.prototype.onCollisionEnter = function(other, self) {
        if (this.rocketTriggered && other.node.getComponent("popper") && !other.node.getComponent("popper").popperDestroyed) {
          other.node.getComponent("popper").onPopperTapped();
          window.clearInterval(this.rocketMove);
          self.node.destroy();
        }
      };
      collider.prototype.onCollisionStay = function(other, self) {};
      collider.prototype.onCollisionExit = function(other, self) {
        this.rocketTriggered = true;
      };
      __decorate([ property(cc.BoxCollider) ], collider.prototype, "collider", void 0);
      collider = __decorate([ ccclass ], collider);
      return collider;
    }(cc.Component);
    exports.default = collider;
    cc._RF.pop();
  }, {} ],
  gameController: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "bce8c4j/pRE16TGUHmYYw3D", "gameController");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var gridItemManager_1 = require("./gridItemManager");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var gameController = function(_super) {
      __extends(gameController, _super);
      function gameController() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.rowSize = 5;
        _this.columnSize = 6;
        _this.gridItem = null;
        _this.gridArea = null;
        _this.gridItemManager = null;
        _this.levelUp = null;
        _this.gameOver = null;
        _this.level = null;
        _this.gridPos = [];
        _this.userLevel = 1;
        return _this;
      }
      gameController.prototype.onLoad = function() {
        this.createGrid();
      };
      gameController.prototype.start = function() {
        this.level.string = this.userLevel.toString();
        this.gridItemManager.initializeGamePlay(this);
      };
      gameController.prototype.createGrid = function() {
        this.gridArea.width = this.gridItem.width * this.columnSize;
        this.gridArea.height = this.gridItem.height * this.rowSize;
        var currItemPos = this.gridItem.getPosition();
        for (var i = 0; i < this.rowSize; i++) for (var j = 0; j < this.columnSize; j++) {
          var itemPos = this.gridItem.getPosition();
          itemPos.x = currItemPos.x + j * this.gridItem.width;
          itemPos.y = currItemPos.y - i * this.gridItem.height;
          this.gridPos.push(itemPos);
        }
      };
      gameController.prototype.getGridPos = function() {
        return this.gridPos;
      };
      gameController.prototype.getGridDimention = function() {
        return {
          row: this.rowSize,
          column: this.columnSize
        };
      };
      gameController.prototype.showLevelUp = function() {
        this.levelUp.runAction(cc.scaleTo(.5, 1, 1));
      };
      gameController.prototype.showGameOver = function() {
        this.gameOver.runAction(cc.scaleTo(.5, 1, 1));
      };
      gameController.prototype.onNextLevelClick = function() {
        this.levelUp.runAction(cc.sequence(cc.scaleTo(.5, 0, 0), cc.callFunc(this.loadNextLevel.bind(this))));
      };
      gameController.prototype.loadNextLevel = function() {
        this.userLevel++;
        this.level.string = this.userLevel.toString();
        this.gridItemManager.createGrid(this.userLevel);
      };
      __decorate([ property ], gameController.prototype, "rowSize", void 0);
      __decorate([ property ], gameController.prototype, "columnSize", void 0);
      __decorate([ property(cc.Node) ], gameController.prototype, "gridItem", void 0);
      __decorate([ property(cc.Node) ], gameController.prototype, "gridArea", void 0);
      __decorate([ property(gridItemManager_1.default) ], gameController.prototype, "gridItemManager", void 0);
      __decorate([ property(cc.Node) ], gameController.prototype, "levelUp", void 0);
      __decorate([ property(cc.Node) ], gameController.prototype, "gameOver", void 0);
      __decorate([ property(cc.Label) ], gameController.prototype, "level", void 0);
      gameController = __decorate([ ccclass ], gameController);
      return gameController;
    }(cc.Component);
    exports.default = gameController;
    cc._RF.pop();
  }, {
    "./gridItemManager": "gridItemManager"
  } ],
  gameDefinitions: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "467532uySFEhb+7DqGvXdJB", "gameDefinitions");
    "use strict";
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.GRID_STATE = void 0;
    var GRID_STATE;
    (function(GRID_STATE) {
      GRID_STATE[GRID_STATE["AVAILABLE"] = 0] = "AVAILABLE";
      GRID_STATE[GRID_STATE["FILLED"] = 1] = "FILLED";
    })(GRID_STATE = exports.GRID_STATE || (exports.GRID_STATE = {}));
    cc._RF.pop();
  }, {} ],
  gridItemManager: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "ae9a6OamCtJcbG08WXQNh/D", "gridItemManager");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var gameDefinitions_1 = require("./gameDefinitions");
    var gridItem_1 = require("./gridItem");
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var gridItemManager = function(_super) {
      __extends(gridItemManager, _super);
      function gridItemManager() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.popperPrefab = null;
        _this.clap = null;
        _this.gameController = null;
        _this.grid = [];
        _this.gameOver = false;
        _this.testarray = [];
        return _this;
      }
      gridItemManager.prototype.onLoad = function() {};
      gridItemManager.prototype.start = function() {};
      gridItemManager.prototype.initializeGamePlay = function(Controller) {
        this.gameController = Controller;
        this.createGrid(1);
      };
      gridItemManager.prototype.createGrid = function(level) {
        this.gameOver = false;
        this.testarray = [];
        var testvar = [];
        1 == level ? this.addItemsToList() : this.resetGrid();
        var gridSize = this.gameController.getGridDimention().column * this.gameController.getGridDimention().row;
        var itemsToSpawn = Math.min(5 * level, gridSize - 5) + Math.floor(5 * Math.random()) + 1;
        for (var i = 0; i < itemsToSpawn; i++) {
          var randomPos = Math.floor(Math.random() * gridSize);
          if (this.grid[randomPos].gridState == gameDefinitions_1.GRID_STATE.AVAILABLE) {
            this.grid[randomPos].gridState = gameDefinitions_1.GRID_STATE.FILLED;
            var popper = cc.instantiate(this.popperPrefab);
            popper.getComponent("popper").init(this.gameController, this.grid[randomPos].gridItemID);
            var currItem = new cc.Node();
            this.grid[randomPos].gridCurrItem = currItem;
            this.grid[randomPos].gridCurrItem.addChild(popper);
            this.grid[randomPos].gridCurrItem.setPosition(this.grid[randomPos].gridPosition);
            this.gameController.gridArea.addChild(this.grid[randomPos].gridCurrItem);
            testvar.push(randomPos);
          } else i--;
        }
        console.log("Items : ", testvar);
      };
      gridItemManager.prototype.addItemsToList = function() {
        var counter = 0;
        for (var row = 0; row < this.gameController.getGridDimention().row; row++) for (var column = 0; column < this.gameController.getGridDimention().column; column++) {
          var newGridItem = new gridItem_1.default();
          newGridItem.gridItemID = counter;
          newGridItem.gridState = gameDefinitions_1.GRID_STATE.AVAILABLE;
          newGridItem.gridPosition = this.gameController.getGridPos()[counter];
          this.grid.push(newGridItem);
          counter++;
        }
      };
      gridItemManager.prototype.removeGridItem = function(itemID) {
        this.grid[itemID].gridState = gameDefinitions_1.GRID_STATE.AVAILABLE;
        this.grid[itemID].gridCurrItem.removeAllChildren();
        this.testarray.includes(itemID) || this.testarray.push(itemID);
        this.checkGameOver();
      };
      gridItemManager.prototype.resetGrid = function() {
        this.grid.forEach(function(item) {
          item.gridState = gameDefinitions_1.GRID_STATE.AVAILABLE;
          item.gridCurrItem = null;
        });
      };
      gridItemManager.prototype.checkGameOver = function() {
        var _this = this;
        var remaining = 0;
        var remaininggrid = 0;
        this.grid.forEach(function(item) {
          if (item.gridState == gameDefinitions_1.GRID_STATE.FILLED) {
            _this.gameOver = false;
            remaining++;
          }
        });
        remaining <= 3 && console.log("Issue have : ", this.testarray);
        if (0 == remaining && false == this.gameOver) {
          console.log("STAGE COMPLETE");
          this.gameOver = true;
          cc.audioEngine.playEffect(this.clap, false);
          this.gameController.userLevel < 5 ? this.gameController.showLevelUp() : this.gameController.showGameOver();
        }
      };
      __decorate([ property(cc.Prefab) ], gridItemManager.prototype, "popperPrefab", void 0);
      __decorate([ property(cc.AudioClip) ], gridItemManager.prototype, "clap", void 0);
      gridItemManager = __decorate([ ccclass ], gridItemManager);
      return gridItemManager;
    }(cc.Component);
    exports.default = gridItemManager;
    cc._RF.pop();
  }, {
    "./gameDefinitions": "gameDefinitions",
    "./gridItem": "gridItem"
  } ],
  gridItem: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "1bd34ZgOsJAC7KVvug/iy+A", "gridItem");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var gridItem = function(_super) {
      __extends(gridItem, _super);
      function gridItem() {
        return null !== _super && _super.apply(this, arguments) || this;
      }
      gridItem.prototype.onLoad = function() {};
      gridItem.prototype.start = function() {};
      gridItem = __decorate([ ccclass ], gridItem);
      return gridItem;
    }(cc.Component);
    exports.default = gridItem;
    cc._RF.pop();
  }, {} ],
  popper: [ function(require, module, exports) {
    "use strict";
    cc._RF.push(module, "997ecnFPMNNBaOcj5hdKsKF", "popper");
    "use strict";
    var __extends = this && this.__extends || function() {
      var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function(d, b) {
          d.__proto__ = b;
        } || function(d, b) {
          for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        };
        return extendStatics(d, b);
      };
      return function(d, b) {
        extendStatics(d, b);
        function __() {
          this.constructor = d;
        }
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
      };
    }();
    var __decorate = this && this.__decorate || function(decorators, target, key, desc) {
      var c = arguments.length, r = c < 3 ? target : null === desc ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
      if ("object" === typeof Reflect && "function" === typeof Reflect.decorate) r = Reflect.decorate(decorators, target, key, desc); else for (var i = decorators.length - 1; i >= 0; i--) (d = decorators[i]) && (r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r);
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
    var popper = function(_super) {
      __extends(popper, _super);
      function popper() {
        var _this = null !== _super && _super.apply(this, arguments) || this;
        _this.popperLevel = 2;
        _this.popperID = null;
        _this.gameController = null;
        _this.popperDestroyed = false;
        _this.popperSprite = null;
        _this.popperExplod = null;
        _this.popperEye = null;
        _this.popperVarients = [];
        _this.rocket = null;
        _this.popperBtn = null;
        _this.popperTap = null;
        return _this;
      }
      popper.prototype.onLoad = function() {};
      popper.prototype.init = function(controller, itemID) {
        this.gameController = controller;
        this.popperID = itemID;
        this.popperLevel = Math.floor(Math.random() * this.popperVarients.length);
      };
      popper.prototype.start = function() {
        this.popperSprite.spriteFrame = this.popperVarients[this.popperLevel];
      };
      popper.prototype.onPopperTapped = function() {
        if (this.popperLevel > 0) {
          cc.audioEngine.playEffect(this.popperTap, false);
          this.popperLevel--;
          this.popperSprite.spriteFrame = this.popperVarients[this.popperLevel];
        } else if (this.popperDestroyed) this.gameController.gridItemManager.removeGridItem(this.popperID); else {
          cc.audioEngine.playEffect(this.popperTap, false);
          this.popperBtn.interactable = false;
          this.popperDestroyed = true;
          this.popperExplod.runAction(cc.sequence(cc.scaleTo(.5, 1, 1), cc.callFunc(this.hidePopper.bind(this)), cc.scaleTo(.5, 0, 0), cc.callFunc(this.onPopperDestroyCallback.bind(this))));
        }
      };
      popper.prototype.hidePopper = function() {
        this.popperSprite.node.active = false;
        this.popperEye.active = false;
      };
      popper.prototype.onPopperDestroyCallback = function() {
        for (var i = 0; i < 4; i++) {
          var rocket = cc.instantiate(this.rocket);
          rocket.getComponent("collider").init(i, this.popperID, this.gameController);
          this.node.addChild(rocket);
        }
      };
      __decorate([ property(cc.Sprite) ], popper.prototype, "popperSprite", void 0);
      __decorate([ property(cc.Node) ], popper.prototype, "popperExplod", void 0);
      __decorate([ property(cc.Node) ], popper.prototype, "popperEye", void 0);
      __decorate([ property(cc.SpriteFrame) ], popper.prototype, "popperVarients", void 0);
      __decorate([ property(cc.Prefab) ], popper.prototype, "rocket", void 0);
      __decorate([ property(cc.Button) ], popper.prototype, "popperBtn", void 0);
      __decorate([ property(cc.AudioClip) ], popper.prototype, "popperTap", void 0);
      popper = __decorate([ ccclass ], popper);
      return popper;
    }(cc.Component);
    exports.default = popper;
    cc._RF.pop();
  }, {} ]
}, {}, [ "collider", "gameController", "gameDefinitions", "gridItem", "gridItemManager", "popper" ]);