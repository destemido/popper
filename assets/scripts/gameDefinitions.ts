export interface GridValue {
    row: number;
    column: number;
}

export enum GRID_STATE {
    AVAILABLE = 0,
    FILLED = 1,
}