import gameController from "./gameController";
import { GRID_STATE } from "./gameDefinitions";
import gridItem from "./gridItem";

const {ccclass, property} = cc._decorator;

@ccclass
export default class gridItemManager extends cc.Component {

    @property(cc.Prefab)
    popperPrefab: cc.Prefab = null;

    @property(cc.AudioClip)
    clap: cc.AudioClip = null;

    private gameController: gameController = null;
    public grid: Array<gridItem> = [];
    private gameOver: boolean = false;
    private testarray: Array<number> = [];

    onLoad () {}

    start () {

    }

    initializeGamePlay(Controller: gameController) {
        this.gameController = Controller;
        this.createGrid(1);
    }

    public createGrid(level: number) {
        this.gameOver = false;
        this.testarray = [];
        let testvar = [];
        if (level == 1) {
            this.addItemsToList();
        } else {
            this.resetGrid();
        }
        let gridSize = this.gameController.getGridDimention().column * this.gameController.getGridDimention().row;
        let itemsToSpawn = Math.min(level * 5, gridSize - 5) + Math.floor(Math.random() * 5) + 1;
        for (let i = 0; i < itemsToSpawn; i++) {
            let randomPos = Math.floor(Math.random() * gridSize);
            if (this.grid[randomPos].gridState == GRID_STATE.AVAILABLE) {
                this.grid[randomPos].gridState = GRID_STATE.FILLED;
                let popper = cc.instantiate(this.popperPrefab);
                popper.getComponent("popper").init(this.gameController, this.grid[randomPos].gridItemID);
                let currItem = new cc.Node();
                this.grid[randomPos].gridCurrItem = currItem;
                this.grid[randomPos].gridCurrItem.addChild(popper);
                this.grid[randomPos].gridCurrItem.setPosition(this.grid[randomPos].gridPosition);
                this.gameController.gridArea.addChild(this.grid[randomPos].gridCurrItem);
                testvar.push(randomPos);;
            } else {
                i--;
            }
        }
        console.log("Items : ", testvar);
    }

    private addItemsToList() {
        let counter = 0;
        for (let row = 0; row < this.gameController.getGridDimention().row; row++) {
            for (let column = 0; column < this.gameController.getGridDimention().column; column++) {
                var newGridItem: gridItem = new gridItem();
                newGridItem.gridItemID = counter;
                newGridItem.gridState = GRID_STATE.AVAILABLE;
                newGridItem.gridPosition = this.gameController.getGridPos()[counter];
                this.grid.push(newGridItem);
                counter++;
            }
        }
    }

    removeGridItem(itemID: number) {
        this.grid[itemID].gridState = GRID_STATE.AVAILABLE;
        this.grid[itemID].gridCurrItem.removeAllChildren();
        if (!this.testarray.includes(itemID)) {
            this.testarray.push(itemID);
        }
        this.checkGameOver();
    }

    private resetGrid() {
        this.grid.forEach(item => {
            item.gridState = GRID_STATE.AVAILABLE;
            item.gridCurrItem = null;
        });
    }

    private checkGameOver() {
        let remaining = 0;
        let remaininggrid = 0;
        this.grid.forEach(item => {
            if (item.gridState == GRID_STATE.FILLED) {
                this.gameOver = false;
                remaining++;
            }
        });
        if (remaining <= 3) {
            // this.grid.forEach(item => {
            //     if (item.gridCurrItem && item.gridCurrItem.children.length != 0) {
            //         remaininggrid++;
            //         if (remaininggrid == 1) {
            //             console.log(item);
            //         }
            //     }
            // });
            console.log("Issue have : ",this.testarray);
        }
        // console.log("Remaining : "+remaining+" : "+remaininggrid);
        if (remaining == 0 && this.gameOver == false) {
            console.log("STAGE COMPLETE");
            this.gameOver = true;
            // this.gameController.gridArea.removeAllChildren();
            cc.audioEngine.playEffect(this.clap, false);
            if (this.gameController.userLevel < 5) {
                this.gameController.showLevelUp();
            } else {
                this.gameController.showGameOver();
            }
        }
    }

    // update (dt) {}
}
