import { GRID_STATE } from "./gameDefinitions";

const {ccclass, property} = cc._decorator;

@ccclass
export default class gridItem extends cc.Component {

    public gridItemID: number;
    public gridPosition: cc.Vec2;
    public gridState: GRID_STATE;
    public gridCurrItem: cc.Node;

    onLoad () {}

    start () {

    }

    // update (dt) {}
}
