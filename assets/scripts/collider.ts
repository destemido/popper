import gameController from "./gameController";

const {ccclass, property} = cc._decorator;

@ccclass
export default class collider extends cc.Component {

    private gameController: gameController;
    private popperID: number = null;
    private colliderDirection: number = null;
    private rocketMove: number = null;
    private rocketTriggered: boolean = false;

    @property(cc.BoxCollider)
    collider: cc.BoxCollider = null;

    // onLoad () {}

    start () {
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
    }

    init(direction: number, popperID: number, controller: gameController) {
        this.colliderDirection = direction;
        this.popperID = popperID;
        this.gameController = controller;
        let popperChild = this.gameController.gridItemManager.grid[this.popperID].gridCurrItem.getChildByName("popper");
        if (direction == 0) {
            let maxDistance = this.node.x + this.gameController.gridArea.width*2;
            this.rocketMove = window.setInterval(() => {
                this.node.x += 3;
                if (this.node.x > maxDistance) {
                    window.clearInterval(this.rocketMove);
                    if (popperChild) {
                        popperChild.getComponent("popper").onPopperTapped();
                    } else {
                        console.log("Error...");
                    }
                    this.destroy();
                }
            }, 10);
        } else if (direction == 1) {
            let maxDistance = this.node.y + this.gameController.gridArea.height*2;
            this.rocketMove = window.setInterval(() => {
                this.node.y += 3;
                if (this.node.y > maxDistance) {
                    window.clearInterval(this.rocketMove);
                    if (popperChild) {
                        popperChild.getComponent("popper").onPopperTapped();
                    } else {
                        console.log("Error...");
                    }
                    this.destroy();
                }
            }, 10);
        } else if (direction == 2) {
            let maxDistance = this.node.x - this.gameController.gridArea.width*2;
            this.rocketMove = window.setInterval(() => {
                this.node.x -= 3;
                if (this.node.x < maxDistance) {
                    window.clearInterval(this.rocketMove);
                    if (popperChild) {
                        popperChild.getComponent("popper").onPopperTapped();
                    } else {
                        console.log("Error...");
                    }
                    this.destroy();
                }
            }, 10);
        } else {
            let maxDistance = this.node.y - this.gameController.gridArea.height*2;
            this.rocketMove = window.setInterval(() => {
                this.node.y -= 3;
                if (this.node.y < maxDistance) {
                    window.clearInterval(this.rocketMove);
                    if (popperChild) {
                        popperChild.getComponent("popper").onPopperTapped();
                    } else {
                        console.log("Error...");
                    }
                    this.destroy();
                }
            }, 10);
        }
    }

    onCollisionEnter (other: any, self: any) {
        if (this.rocketTriggered && other.node.getComponent("popper") && !other.node.getComponent("popper").popperDestroyed) {
            other.node.getComponent("popper").onPopperTapped();
            window.clearInterval(this.rocketMove);
            self.node.destroy();
            // this.gameController.gridItemManager.removeGridItem(this.popperID);
        }
    }
    
    onCollisionStay (other: any, self: any) {
    }
    
    onCollisionExit (other: any, self: any) {
        this.rocketTriggered = true;
    }

    // update (dt) {}
}
