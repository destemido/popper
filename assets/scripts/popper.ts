import gameController from "./gameController";

const {ccclass, property} = cc._decorator;

@ccclass
export default class popper extends cc.Component {

    private popperLevel: number = 2;
    private popperID: number = null;
    private gameController: gameController = null;
    public popperDestroyed: boolean = false;

    @property(cc.Sprite)
    popperSprite: cc.Sprite = null;

    @property(cc.Node)
    popperExplod: cc.Node = null;

    @property(cc.Node)
    popperEye: cc.Node = null;

    @property(cc.SpriteFrame)
    popperVarients: cc.SpriteFrame[] = [];

    @property(cc.Prefab)
    rocket: cc.Prefab = null;

    @property(cc.Button)
    popperBtn: cc.Button = null;

    @property(cc.AudioClip)
    popperTap: cc.AudioClip = null;

    onLoad () {}

    init (controller: gameController, itemID: number) {
        this.gameController = controller;
        this.popperID = itemID;
        this.popperLevel = Math.floor(Math.random() * this.popperVarients.length);
    }

    start () {
        this.popperSprite.spriteFrame = this.popperVarients[this.popperLevel];
    }

    onPopperTapped() {
        if (this.popperLevel > 0) {
            cc.audioEngine.playEffect(this.popperTap, false);
            this.popperLevel--;
            this.popperSprite.spriteFrame = this.popperVarients[this.popperLevel];
        } else if (!this.popperDestroyed) {
            cc.audioEngine.playEffect(this.popperTap, false);
            this.popperBtn.interactable = false;
            this.popperDestroyed = true;
            this.popperExplod.runAction(
                cc.sequence(
                    cc.scaleTo(0.5, 1, 1),
                    cc.callFunc(this.hidePopper.bind(this)),
                    cc.scaleTo(0.5, 0, 0),
                    cc.callFunc(this.onPopperDestroyCallback.bind(this))
                )
            )
        } else {
            this.gameController.gridItemManager.removeGridItem(this.popperID);
        }
    }

    hidePopper() {
        this.popperSprite.node.active = false;
        this.popperEye.active = false;
    }

    onPopperDestroyCallback() {
        for (let i = 0; i < 4; i++) {
            let rocket = cc.instantiate(this.rocket);
            rocket.getComponent("collider").init(i, this.popperID, this.gameController);
            this.node.addChild(rocket);
        }  
    }

    // update (dt) {}
}
