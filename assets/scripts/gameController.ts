import gridItemManager from "./gridItemManager";
import { GridValue } from "./gameDefinitions";

//app password : FAAprgF2rs2WKjH4j54p

const {ccclass, property} = cc._decorator;

@ccclass
export default class gameController extends cc.Component {

    @property
    rowSize: number = 5;

    @property
    columnSize: number = 6;

    @property(cc.Node)
    gridItem: cc.Node = null;

    @property(cc.Node)
    gridArea: cc.Node = null;

    @property(gridItemManager)
    gridItemManager: gridItemManager = null;

    @property(cc.Node)
    levelUp: cc.Node = null;

    @property(cc.Node)
    gameOver: cc.Node = null;

    @property(cc.Label)
    level: cc.Label = null;

    private gridPos:cc.Vec2[] = [];
    public userLevel: number = 1;


    onLoad () {
        this.createGrid();
    }

    start () {
        this.level.string = this.userLevel.toString();
        this.gridItemManager.initializeGamePlay(this);
    }

    private createGrid() {
        this.gridArea.width = this.gridItem.width * this.columnSize;
        this.gridArea.height = this.gridItem.height * this.rowSize;
        const currItemPos = this.gridItem.getPosition();
        
        for (let i = 0; i < this.rowSize; i++) {
            for (let j = 0; j < this.columnSize; j++) {
                let itemPos:cc.Vec2 = this.gridItem.getPosition();
                itemPos.x = currItemPos.x + j * this.gridItem.width;
                itemPos.y = currItemPos.y - i * this.gridItem.height;
                this.gridPos.push(itemPos);
            }
        }
    }

    getGridPos(): cc.Vec2[] {
        return this.gridPos;
    }

    getGridDimention(): GridValue {
        return {
            row: this.rowSize,
            column: this.columnSize
        }
    }

    showLevelUp() {
        this.levelUp.runAction(
            cc.scaleTo(0.5, 1, 1)
        )
    }

    showGameOver() {
        // this.gameOver.runAction(
        //     cc.sequence(
        //         cc.scaleTo(0.5, 1, 1),
        //         cc.scaleTo(0.5, 0, 0),
        //         cc.callFunc(() => {
        //             cc.director.loadScene("game");
        //         })
        //     )
        // );

        this.gameOver.runAction(
            cc.scaleTo(0.5, 1, 1)
        );
    }

    onNextLevelClick() {
        this.levelUp.runAction(
            cc.sequence(
                cc.scaleTo(0.5, 0, 0),
                cc.callFunc(this.loadNextLevel.bind(this))
            )
        )
    }

    loadNextLevel() {
        this.userLevel++;
        this.level.string = this.userLevel.toString();
        this.gridItemManager.createGrid(this.userLevel);
    }

    // update (dt) {}
}
