"use strict";
cc._RF.push(module, '467532uySFEhb+7DqGvXdJB', 'gameDefinitions');
// scripts/gameDefinitions.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GRID_STATE = void 0;
var GRID_STATE;
(function (GRID_STATE) {
    GRID_STATE[GRID_STATE["AVAILABLE"] = 0] = "AVAILABLE";
    GRID_STATE[GRID_STATE["FILLED"] = 1] = "FILLED";
})(GRID_STATE = exports.GRID_STATE || (exports.GRID_STATE = {}));

cc._RF.pop();