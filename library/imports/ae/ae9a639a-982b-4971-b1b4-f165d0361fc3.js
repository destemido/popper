"use strict";
cc._RF.push(module, 'ae9a6OamCtJcbG08WXQNh/D', 'gridItemManager');
// scripts/gridItemManager.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var gameDefinitions_1 = require("./gameDefinitions");
var gridItem_1 = require("./gridItem");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var gridItemManager = /** @class */ (function (_super) {
    __extends(gridItemManager, _super);
    function gridItemManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.popperPrefab = null;
        _this.clap = null;
        _this.gameController = null;
        _this.grid = [];
        _this.gameOver = false;
        _this.testarray = [];
        return _this;
        // update (dt) {}
    }
    gridItemManager.prototype.onLoad = function () { };
    gridItemManager.prototype.start = function () {
    };
    gridItemManager.prototype.initializeGamePlay = function (Controller) {
        this.gameController = Controller;
        this.createGrid(1);
    };
    gridItemManager.prototype.createGrid = function (level) {
        this.gameOver = false;
        this.testarray = [];
        var testvar = [];
        if (level == 1) {
            this.addItemsToList();
        }
        else {
            this.resetGrid();
        }
        var gridSize = this.gameController.getGridDimention().column * this.gameController.getGridDimention().row;
        var itemsToSpawn = Math.min(level * 5, gridSize - 5) + Math.floor(Math.random() * 5) + 1;
        for (var i = 0; i < itemsToSpawn; i++) {
            var randomPos = Math.floor(Math.random() * gridSize);
            if (this.grid[randomPos].gridState == gameDefinitions_1.GRID_STATE.AVAILABLE) {
                this.grid[randomPos].gridState = gameDefinitions_1.GRID_STATE.FILLED;
                var popper = cc.instantiate(this.popperPrefab);
                popper.getComponent("popper").init(this.gameController, this.grid[randomPos].gridItemID);
                var currItem = new cc.Node();
                this.grid[randomPos].gridCurrItem = currItem;
                this.grid[randomPos].gridCurrItem.addChild(popper);
                this.grid[randomPos].gridCurrItem.setPosition(this.grid[randomPos].gridPosition);
                this.gameController.gridArea.addChild(this.grid[randomPos].gridCurrItem);
                testvar.push(randomPos);
                ;
            }
            else {
                i--;
            }
        }
        console.log("Items : ", testvar);
    };
    gridItemManager.prototype.addItemsToList = function () {
        var counter = 0;
        for (var row = 0; row < this.gameController.getGridDimention().row; row++) {
            for (var column = 0; column < this.gameController.getGridDimention().column; column++) {
                var newGridItem = new gridItem_1.default();
                newGridItem.gridItemID = counter;
                newGridItem.gridState = gameDefinitions_1.GRID_STATE.AVAILABLE;
                newGridItem.gridPosition = this.gameController.getGridPos()[counter];
                this.grid.push(newGridItem);
                counter++;
            }
        }
    };
    gridItemManager.prototype.removeGridItem = function (itemID) {
        this.grid[itemID].gridState = gameDefinitions_1.GRID_STATE.AVAILABLE;
        this.grid[itemID].gridCurrItem.removeAllChildren();
        if (!this.testarray.includes(itemID)) {
            this.testarray.push(itemID);
        }
        this.checkGameOver();
    };
    gridItemManager.prototype.resetGrid = function () {
        this.grid.forEach(function (item) {
            item.gridState = gameDefinitions_1.GRID_STATE.AVAILABLE;
            item.gridCurrItem = null;
        });
    };
    gridItemManager.prototype.checkGameOver = function () {
        var _this = this;
        var remaining = 0;
        var remaininggrid = 0;
        this.grid.forEach(function (item) {
            if (item.gridState == gameDefinitions_1.GRID_STATE.FILLED) {
                _this.gameOver = false;
                remaining++;
            }
        });
        if (remaining <= 3) {
            // this.grid.forEach(item => {
            //     if (item.gridCurrItem && item.gridCurrItem.children.length != 0) {
            //         remaininggrid++;
            //         if (remaininggrid == 1) {
            //             console.log(item);
            //         }
            //     }
            // });
            console.log("Issue have : ", this.testarray);
        }
        // console.log("Remaining : "+remaining+" : "+remaininggrid);
        if (remaining == 0 && this.gameOver == false) {
            console.log("STAGE COMPLETE");
            this.gameOver = true;
            // this.gameController.gridArea.removeAllChildren();
            cc.audioEngine.playEffect(this.clap, false);
            if (this.gameController.userLevel < 5) {
                this.gameController.showLevelUp();
            }
            else {
                this.gameController.showGameOver();
            }
        }
    };
    __decorate([
        property(cc.Prefab)
    ], gridItemManager.prototype, "popperPrefab", void 0);
    __decorate([
        property(cc.AudioClip)
    ], gridItemManager.prototype, "clap", void 0);
    gridItemManager = __decorate([
        ccclass
    ], gridItemManager);
    return gridItemManager;
}(cc.Component));
exports.default = gridItemManager;

cc._RF.pop();