"use strict";
cc._RF.push(module, '21deazBcsNDKZuMcwda1EIx', 'collider');
// scripts/collider.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var collider = /** @class */ (function (_super) {
    __extends(collider, _super);
    function collider() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.popperID = null;
        _this.colliderDirection = null;
        _this.rocketMove = null;
        _this.rocketTriggered = false;
        _this.collider = null;
        return _this;
        // update (dt) {}
    }
    // onLoad () {}
    collider.prototype.start = function () {
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
    };
    collider.prototype.init = function (direction, popperID, controller) {
        var _this = this;
        this.colliderDirection = direction;
        this.popperID = popperID;
        this.gameController = controller;
        var popperChild = this.gameController.gridItemManager.grid[this.popperID].gridCurrItem.getChildByName("popper");
        if (direction == 0) {
            var maxDistance_1 = this.node.x + this.gameController.gridArea.width * 2;
            this.rocketMove = window.setInterval(function () {
                _this.node.x += 3;
                if (_this.node.x > maxDistance_1) {
                    window.clearInterval(_this.rocketMove);
                    if (popperChild) {
                        popperChild.getComponent("popper").onPopperTapped();
                    }
                    else {
                        console.log("Error...");
                    }
                    _this.destroy();
                }
            }, 10);
        }
        else if (direction == 1) {
            var maxDistance_2 = this.node.y + this.gameController.gridArea.height * 2;
            this.rocketMove = window.setInterval(function () {
                _this.node.y += 3;
                if (_this.node.y > maxDistance_2) {
                    window.clearInterval(_this.rocketMove);
                    if (popperChild) {
                        popperChild.getComponent("popper").onPopperTapped();
                    }
                    else {
                        console.log("Error...");
                    }
                    _this.destroy();
                }
            }, 10);
        }
        else if (direction == 2) {
            var maxDistance_3 = this.node.x - this.gameController.gridArea.width * 2;
            this.rocketMove = window.setInterval(function () {
                _this.node.x -= 3;
                if (_this.node.x < maxDistance_3) {
                    window.clearInterval(_this.rocketMove);
                    if (popperChild) {
                        popperChild.getComponent("popper").onPopperTapped();
                    }
                    else {
                        console.log("Error...");
                    }
                    _this.destroy();
                }
            }, 10);
        }
        else {
            var maxDistance_4 = this.node.y - this.gameController.gridArea.height * 2;
            this.rocketMove = window.setInterval(function () {
                _this.node.y -= 3;
                if (_this.node.y < maxDistance_4) {
                    window.clearInterval(_this.rocketMove);
                    if (popperChild) {
                        popperChild.getComponent("popper").onPopperTapped();
                    }
                    else {
                        console.log("Error...");
                    }
                    _this.destroy();
                }
            }, 10);
        }
    };
    collider.prototype.onCollisionEnter = function (other, self) {
        if (this.rocketTriggered && other.node.getComponent("popper") && !other.node.getComponent("popper").popperDestroyed) {
            other.node.getComponent("popper").onPopperTapped();
            window.clearInterval(this.rocketMove);
            self.node.destroy();
            // this.gameController.gridItemManager.removeGridItem(this.popperID);
        }
    };
    collider.prototype.onCollisionStay = function (other, self) {
    };
    collider.prototype.onCollisionExit = function (other, self) {
        this.rocketTriggered = true;
    };
    __decorate([
        property(cc.BoxCollider)
    ], collider.prototype, "collider", void 0);
    collider = __decorate([
        ccclass
    ], collider);
    return collider;
}(cc.Component));
exports.default = collider;

cc._RF.pop();