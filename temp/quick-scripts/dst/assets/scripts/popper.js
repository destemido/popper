
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/popper.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '997ecnFPMNNBaOcj5hdKsKF', 'popper');
// scripts/popper.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var popper = /** @class */ (function (_super) {
    __extends(popper, _super);
    function popper() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.popperLevel = 2;
        _this.popperID = null;
        _this.gameController = null;
        _this.popperDestroyed = false;
        _this.popperSprite = null;
        _this.popperExplod = null;
        _this.popperEye = null;
        _this.popperVarients = [];
        _this.rocket = null;
        _this.popperBtn = null;
        _this.popperTap = null;
        return _this;
        // update (dt) {}
    }
    popper.prototype.onLoad = function () { };
    popper.prototype.init = function (controller, itemID) {
        this.gameController = controller;
        this.popperID = itemID;
        this.popperLevel = Math.floor(Math.random() * this.popperVarients.length);
    };
    popper.prototype.start = function () {
        this.popperSprite.spriteFrame = this.popperVarients[this.popperLevel];
    };
    popper.prototype.onPopperTapped = function () {
        if (this.popperLevel > 0) {
            cc.audioEngine.playEffect(this.popperTap, false);
            this.popperLevel--;
            this.popperSprite.spriteFrame = this.popperVarients[this.popperLevel];
        }
        else if (!this.popperDestroyed) {
            cc.audioEngine.playEffect(this.popperTap, false);
            this.popperBtn.interactable = false;
            this.popperDestroyed = true;
            this.popperExplod.runAction(cc.sequence(cc.scaleTo(0.5, 1, 1), cc.callFunc(this.hidePopper.bind(this)), cc.scaleTo(0.5, 0, 0), cc.callFunc(this.onPopperDestroyCallback.bind(this))));
        }
        else {
            this.gameController.gridItemManager.removeGridItem(this.popperID);
        }
    };
    popper.prototype.hidePopper = function () {
        this.popperSprite.node.active = false;
        this.popperEye.active = false;
    };
    popper.prototype.onPopperDestroyCallback = function () {
        for (var i = 0; i < 4; i++) {
            var rocket = cc.instantiate(this.rocket);
            rocket.getComponent("collider").init(i, this.popperID, this.gameController);
            this.node.addChild(rocket);
        }
    };
    __decorate([
        property(cc.Sprite)
    ], popper.prototype, "popperSprite", void 0);
    __decorate([
        property(cc.Node)
    ], popper.prototype, "popperExplod", void 0);
    __decorate([
        property(cc.Node)
    ], popper.prototype, "popperEye", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], popper.prototype, "popperVarients", void 0);
    __decorate([
        property(cc.Prefab)
    ], popper.prototype, "rocket", void 0);
    __decorate([
        property(cc.Button)
    ], popper.prototype, "popperBtn", void 0);
    __decorate([
        property(cc.AudioClip)
    ], popper.prototype, "popperTap", void 0);
    popper = __decorate([
        ccclass
    ], popper);
    return popper;
}(cc.Component));
exports.default = popper;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL3BvcHBlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFTSxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUcxQztJQUFvQywwQkFBWTtJQUFoRDtRQUFBLHFFQTRFQztRQTFFVyxpQkFBVyxHQUFXLENBQUMsQ0FBQztRQUN4QixjQUFRLEdBQVcsSUFBSSxDQUFDO1FBQ3hCLG9CQUFjLEdBQW1CLElBQUksQ0FBQztRQUN2QyxxQkFBZSxHQUFZLEtBQUssQ0FBQztRQUd4QyxrQkFBWSxHQUFjLElBQUksQ0FBQztRQUcvQixrQkFBWSxHQUFZLElBQUksQ0FBQztRQUc3QixlQUFTLEdBQVksSUFBSSxDQUFDO1FBRzFCLG9CQUFjLEdBQXFCLEVBQUUsQ0FBQztRQUd0QyxZQUFNLEdBQWMsSUFBSSxDQUFDO1FBR3pCLGVBQVMsR0FBYyxJQUFJLENBQUM7UUFHNUIsZUFBUyxHQUFpQixJQUFJLENBQUM7O1FBaUQvQixpQkFBaUI7SUFDckIsQ0FBQztJQWhERyx1QkFBTSxHQUFOLGNBQVcsQ0FBQztJQUVaLHFCQUFJLEdBQUosVUFBTSxVQUEwQixFQUFFLE1BQWM7UUFDNUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxVQUFVLENBQUM7UUFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUM7UUFDdkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzlFLENBQUM7SUFFRCxzQkFBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDMUUsQ0FBQztJQUVELCtCQUFjLEdBQWQ7UUFDSSxJQUFJLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxFQUFFO1lBQ3RCLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDakQsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ25CLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQ3pFO2FBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDOUIsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNqRCxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7WUFDcEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7WUFDNUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQ3ZCLEVBQUUsQ0FBQyxRQUFRLENBQ1AsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUNyQixFQUFFLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQ3ZDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFDckIsRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQ3ZELENBQ0osQ0FBQTtTQUNKO2FBQU07WUFDSCxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3JFO0lBQ0wsQ0FBQztJQUVELDJCQUFVLEdBQVY7UUFDSSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUNsQyxDQUFDO0lBRUQsd0NBQXVCLEdBQXZCO1FBQ0ksS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN4QixJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN6QyxNQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDNUUsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDOUI7SUFDTCxDQUFDO0lBakVEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7Z0RBQ1c7SUFHL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztnREFDVztJQUc3QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzZDQUNRO0lBRzFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7a0RBQ2E7SUFHdEM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzswQ0FDSztJQUd6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzZDQUNRO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUM7NkNBQ1E7SUExQmQsTUFBTTtRQUQxQixPQUFPO09BQ2EsTUFBTSxDQTRFMUI7SUFBRCxhQUFDO0NBNUVELEFBNEVDLENBNUVtQyxFQUFFLENBQUMsU0FBUyxHQTRFL0M7a0JBNUVvQixNQUFNIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGdhbWVDb250cm9sbGVyIGZyb20gXCIuL2dhbWVDb250cm9sbGVyXCI7XG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgcG9wcGVyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblxuICAgIHByaXZhdGUgcG9wcGVyTGV2ZWw6IG51bWJlciA9IDI7XG4gICAgcHJpdmF0ZSBwb3BwZXJJRDogbnVtYmVyID0gbnVsbDtcbiAgICBwcml2YXRlIGdhbWVDb250cm9sbGVyOiBnYW1lQ29udHJvbGxlciA9IG51bGw7XG4gICAgcHVibGljIHBvcHBlckRlc3Ryb3llZDogYm9vbGVhbiA9IGZhbHNlO1xuXG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZSlcbiAgICBwb3BwZXJTcHJpdGU6IGNjLlNwcml0ZSA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICBwb3BwZXJFeHBsb2Q6IGNjLk5vZGUgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgcG9wcGVyRXllOiBjYy5Ob2RlID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5TcHJpdGVGcmFtZSlcbiAgICBwb3BwZXJWYXJpZW50czogY2MuU3ByaXRlRnJhbWVbXSA9IFtdO1xuXG4gICAgQHByb3BlcnR5KGNjLlByZWZhYilcbiAgICByb2NrZXQ6IGNjLlByZWZhYiA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuQnV0dG9uKVxuICAgIHBvcHBlckJ0bjogY2MuQnV0dG9uID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5BdWRpb0NsaXApXG4gICAgcG9wcGVyVGFwOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xuXG4gICAgb25Mb2FkICgpIHt9XG5cbiAgICBpbml0IChjb250cm9sbGVyOiBnYW1lQ29udHJvbGxlciwgaXRlbUlEOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5nYW1lQ29udHJvbGxlciA9IGNvbnRyb2xsZXI7XG4gICAgICAgIHRoaXMucG9wcGVySUQgPSBpdGVtSUQ7XG4gICAgICAgIHRoaXMucG9wcGVyTGV2ZWwgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiB0aGlzLnBvcHBlclZhcmllbnRzLmxlbmd0aCk7XG4gICAgfVxuXG4gICAgc3RhcnQgKCkge1xuICAgICAgICB0aGlzLnBvcHBlclNwcml0ZS5zcHJpdGVGcmFtZSA9IHRoaXMucG9wcGVyVmFyaWVudHNbdGhpcy5wb3BwZXJMZXZlbF07XG4gICAgfVxuXG4gICAgb25Qb3BwZXJUYXBwZWQoKSB7XG4gICAgICAgIGlmICh0aGlzLnBvcHBlckxldmVsID4gMCkge1xuICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdCh0aGlzLnBvcHBlclRhcCwgZmFsc2UpO1xuICAgICAgICAgICAgdGhpcy5wb3BwZXJMZXZlbC0tO1xuICAgICAgICAgICAgdGhpcy5wb3BwZXJTcHJpdGUuc3ByaXRlRnJhbWUgPSB0aGlzLnBvcHBlclZhcmllbnRzW3RoaXMucG9wcGVyTGV2ZWxdO1xuICAgICAgICB9IGVsc2UgaWYgKCF0aGlzLnBvcHBlckRlc3Ryb3llZCkge1xuICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdCh0aGlzLnBvcHBlclRhcCwgZmFsc2UpO1xuICAgICAgICAgICAgdGhpcy5wb3BwZXJCdG4uaW50ZXJhY3RhYmxlID0gZmFsc2U7XG4gICAgICAgICAgICB0aGlzLnBvcHBlckRlc3Ryb3llZCA9IHRydWU7XG4gICAgICAgICAgICB0aGlzLnBvcHBlckV4cGxvZC5ydW5BY3Rpb24oXG4gICAgICAgICAgICAgICAgY2Muc2VxdWVuY2UoXG4gICAgICAgICAgICAgICAgICAgIGNjLnNjYWxlVG8oMC41LCAxLCAxKSxcbiAgICAgICAgICAgICAgICAgICAgY2MuY2FsbEZ1bmModGhpcy5oaWRlUG9wcGVyLmJpbmQodGhpcykpLFxuICAgICAgICAgICAgICAgICAgICBjYy5zY2FsZVRvKDAuNSwgMCwgMCksXG4gICAgICAgICAgICAgICAgICAgIGNjLmNhbGxGdW5jKHRoaXMub25Qb3BwZXJEZXN0cm95Q2FsbGJhY2suYmluZCh0aGlzKSlcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmdhbWVDb250cm9sbGVyLmdyaWRJdGVtTWFuYWdlci5yZW1vdmVHcmlkSXRlbSh0aGlzLnBvcHBlcklEKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGhpZGVQb3BwZXIoKSB7XG4gICAgICAgIHRoaXMucG9wcGVyU3ByaXRlLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIHRoaXMucG9wcGVyRXllLmFjdGl2ZSA9IGZhbHNlO1xuICAgIH1cblxuICAgIG9uUG9wcGVyRGVzdHJveUNhbGxiYWNrKCkge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IDQ7IGkrKykge1xuICAgICAgICAgICAgbGV0IHJvY2tldCA9IGNjLmluc3RhbnRpYXRlKHRoaXMucm9ja2V0KTtcbiAgICAgICAgICAgIHJvY2tldC5nZXRDb21wb25lbnQoXCJjb2xsaWRlclwiKS5pbml0KGksIHRoaXMucG9wcGVySUQsIHRoaXMuZ2FtZUNvbnRyb2xsZXIpO1xuICAgICAgICAgICAgdGhpcy5ub2RlLmFkZENoaWxkKHJvY2tldCk7XG4gICAgICAgIH0gIFxuICAgIH1cblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XG59XG4iXX0=