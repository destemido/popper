
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/collider.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '21deazBcsNDKZuMcwda1EIx', 'collider');
// scripts/collider.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var collider = /** @class */ (function (_super) {
    __extends(collider, _super);
    function collider() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.popperID = null;
        _this.colliderDirection = null;
        _this.rocketMove = null;
        _this.rocketTriggered = false;
        _this.collider = null;
        return _this;
        // update (dt) {}
    }
    // onLoad () {}
    collider.prototype.start = function () {
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
    };
    collider.prototype.init = function (direction, popperID, controller) {
        var _this = this;
        this.colliderDirection = direction;
        this.popperID = popperID;
        this.gameController = controller;
        var popperChild = this.gameController.gridItemManager.grid[this.popperID].gridCurrItem.getChildByName("popper");
        if (direction == 0) {
            var maxDistance_1 = this.node.x + this.gameController.gridArea.width * 2;
            this.rocketMove = window.setInterval(function () {
                _this.node.x += 3;
                if (_this.node.x > maxDistance_1) {
                    window.clearInterval(_this.rocketMove);
                    if (popperChild) {
                        popperChild.getComponent("popper").onPopperTapped();
                    }
                    else {
                        console.log("Error...");
                    }
                    _this.destroy();
                }
            }, 10);
        }
        else if (direction == 1) {
            var maxDistance_2 = this.node.y + this.gameController.gridArea.height * 2;
            this.rocketMove = window.setInterval(function () {
                _this.node.y += 3;
                if (_this.node.y > maxDistance_2) {
                    window.clearInterval(_this.rocketMove);
                    if (popperChild) {
                        popperChild.getComponent("popper").onPopperTapped();
                    }
                    else {
                        console.log("Error...");
                    }
                    _this.destroy();
                }
            }, 10);
        }
        else if (direction == 2) {
            var maxDistance_3 = this.node.x - this.gameController.gridArea.width * 2;
            this.rocketMove = window.setInterval(function () {
                _this.node.x -= 3;
                if (_this.node.x < maxDistance_3) {
                    window.clearInterval(_this.rocketMove);
                    if (popperChild) {
                        popperChild.getComponent("popper").onPopperTapped();
                    }
                    else {
                        console.log("Error...");
                    }
                    _this.destroy();
                }
            }, 10);
        }
        else {
            var maxDistance_4 = this.node.y - this.gameController.gridArea.height * 2;
            this.rocketMove = window.setInterval(function () {
                _this.node.y -= 3;
                if (_this.node.y < maxDistance_4) {
                    window.clearInterval(_this.rocketMove);
                    if (popperChild) {
                        popperChild.getComponent("popper").onPopperTapped();
                    }
                    else {
                        console.log("Error...");
                    }
                    _this.destroy();
                }
            }, 10);
        }
    };
    collider.prototype.onCollisionEnter = function (other, self) {
        if (this.rocketTriggered && other.node.getComponent("popper") && !other.node.getComponent("popper").popperDestroyed) {
            other.node.getComponent("popper").onPopperTapped();
            window.clearInterval(this.rocketMove);
            self.node.destroy();
            // this.gameController.gridItemManager.removeGridItem(this.popperID);
        }
    };
    collider.prototype.onCollisionStay = function (other, self) {
    };
    collider.prototype.onCollisionExit = function (other, self) {
        this.rocketTriggered = true;
    };
    __decorate([
        property(cc.BoxCollider)
    ], collider.prototype, "collider", void 0);
    collider = __decorate([
        ccclass
    ], collider);
    return collider;
}(cc.Component));
exports.default = collider;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL2NvbGxpZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVNLElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQXNDLDRCQUFZO0lBQWxEO1FBQUEscUVBbUdDO1FBaEdXLGNBQVEsR0FBVyxJQUFJLENBQUM7UUFDeEIsdUJBQWlCLEdBQVcsSUFBSSxDQUFDO1FBQ2pDLGdCQUFVLEdBQVcsSUFBSSxDQUFDO1FBQzFCLHFCQUFlLEdBQVksS0FBSyxDQUFDO1FBR3pDLGNBQVEsR0FBbUIsSUFBSSxDQUFDOztRQXlGaEMsaUJBQWlCO0lBQ3JCLENBQUM7SUF4RkcsZUFBZTtJQUVmLHdCQUFLLEdBQUw7UUFDSSxJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDaEQsT0FBTyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7SUFDM0IsQ0FBQztJQUVELHVCQUFJLEdBQUosVUFBSyxTQUFpQixFQUFFLFFBQWdCLEVBQUUsVUFBMEI7UUFBcEUsaUJBOERDO1FBN0RHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxTQUFTLENBQUM7UUFDbkMsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDekIsSUFBSSxDQUFDLGNBQWMsR0FBRyxVQUFVLENBQUM7UUFDakMsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2hILElBQUksU0FBUyxJQUFJLENBQUMsRUFBRTtZQUNoQixJQUFJLGFBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFDO1lBQ3JFLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztnQkFDakMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNqQixJQUFJLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLGFBQVcsRUFBRTtvQkFDM0IsTUFBTSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ3RDLElBQUksV0FBVyxFQUFFO3dCQUNiLFdBQVcsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7cUJBQ3ZEO3lCQUFNO3dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7cUJBQzNCO29CQUNELEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztpQkFDbEI7WUFDTCxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7U0FDVjthQUFNLElBQUksU0FBUyxJQUFJLENBQUMsRUFBRTtZQUN2QixJQUFJLGFBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUMsQ0FBQyxDQUFDO1lBQ3RFLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztnQkFDakMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNqQixJQUFJLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLGFBQVcsRUFBRTtvQkFDM0IsTUFBTSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ3RDLElBQUksV0FBVyxFQUFFO3dCQUNiLFdBQVcsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7cUJBQ3ZEO3lCQUFNO3dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7cUJBQzNCO29CQUNELEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztpQkFDbEI7WUFDTCxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7U0FDVjthQUFNLElBQUksU0FBUyxJQUFJLENBQUMsRUFBRTtZQUN2QixJQUFJLGFBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFDO1lBQ3JFLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztnQkFDakMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNqQixJQUFJLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLGFBQVcsRUFBRTtvQkFDM0IsTUFBTSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ3RDLElBQUksV0FBVyxFQUFFO3dCQUNiLFdBQVcsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7cUJBQ3ZEO3lCQUFNO3dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7cUJBQzNCO29CQUNELEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztpQkFDbEI7WUFDTCxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7U0FDVjthQUFNO1lBQ0gsSUFBSSxhQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFDLENBQUMsQ0FBQztZQUN0RSxJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUM7Z0JBQ2pDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDakIsSUFBSSxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxhQUFXLEVBQUU7b0JBQzNCLE1BQU0sQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUN0QyxJQUFJLFdBQVcsRUFBRTt3QkFDYixXQUFXLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO3FCQUN2RDt5QkFBTTt3QkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO3FCQUMzQjtvQkFDRCxLQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7aUJBQ2xCO1lBQ0wsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1NBQ1Y7SUFDTCxDQUFDO0lBRUQsbUNBQWdCLEdBQWhCLFVBQWtCLEtBQVUsRUFBRSxJQUFTO1FBQ25DLElBQUksSUFBSSxDQUFDLGVBQWUsSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGVBQWUsRUFBRTtZQUNqSCxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUNuRCxNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3BCLHFFQUFxRTtTQUN4RTtJQUNMLENBQUM7SUFFRCxrQ0FBZSxHQUFmLFVBQWlCLEtBQVUsRUFBRSxJQUFTO0lBQ3RDLENBQUM7SUFFRCxrQ0FBZSxHQUFmLFVBQWlCLEtBQVUsRUFBRSxJQUFTO1FBQ2xDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO0lBQ2hDLENBQUM7SUF2RkQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQzs4Q0FDTztJQVRmLFFBQVE7UUFENUIsT0FBTztPQUNhLFFBQVEsQ0FtRzVCO0lBQUQsZUFBQztDQW5HRCxBQW1HQyxDQW5HcUMsRUFBRSxDQUFDLFNBQVMsR0FtR2pEO2tCQW5Hb0IsUUFBUSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBnYW1lQ29udHJvbGxlciBmcm9tIFwiLi9nYW1lQ29udHJvbGxlclwiO1xuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGNvbGxpZGVyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblxuICAgIHByaXZhdGUgZ2FtZUNvbnRyb2xsZXI6IGdhbWVDb250cm9sbGVyO1xuICAgIHByaXZhdGUgcG9wcGVySUQ6IG51bWJlciA9IG51bGw7XG4gICAgcHJpdmF0ZSBjb2xsaWRlckRpcmVjdGlvbjogbnVtYmVyID0gbnVsbDtcbiAgICBwcml2YXRlIHJvY2tldE1vdmU6IG51bWJlciA9IG51bGw7XG4gICAgcHJpdmF0ZSByb2NrZXRUcmlnZ2VyZWQ6IGJvb2xlYW4gPSBmYWxzZTtcblxuICAgIEBwcm9wZXJ0eShjYy5Cb3hDb2xsaWRlcilcbiAgICBjb2xsaWRlcjogY2MuQm94Q29sbGlkZXIgPSBudWxsO1xuXG4gICAgLy8gb25Mb2FkICgpIHt9XG5cbiAgICBzdGFydCAoKSB7XG4gICAgICAgIHZhciBtYW5hZ2VyID0gY2MuZGlyZWN0b3IuZ2V0Q29sbGlzaW9uTWFuYWdlcigpO1xuICAgICAgICBtYW5hZ2VyLmVuYWJsZWQgPSB0cnVlO1xuICAgIH1cblxuICAgIGluaXQoZGlyZWN0aW9uOiBudW1iZXIsIHBvcHBlcklEOiBudW1iZXIsIGNvbnRyb2xsZXI6IGdhbWVDb250cm9sbGVyKSB7XG4gICAgICAgIHRoaXMuY29sbGlkZXJEaXJlY3Rpb24gPSBkaXJlY3Rpb247XG4gICAgICAgIHRoaXMucG9wcGVySUQgPSBwb3BwZXJJRDtcbiAgICAgICAgdGhpcy5nYW1lQ29udHJvbGxlciA9IGNvbnRyb2xsZXI7XG4gICAgICAgIGxldCBwb3BwZXJDaGlsZCA9IHRoaXMuZ2FtZUNvbnRyb2xsZXIuZ3JpZEl0ZW1NYW5hZ2VyLmdyaWRbdGhpcy5wb3BwZXJJRF0uZ3JpZEN1cnJJdGVtLmdldENoaWxkQnlOYW1lKFwicG9wcGVyXCIpO1xuICAgICAgICBpZiAoZGlyZWN0aW9uID09IDApIHtcbiAgICAgICAgICAgIGxldCBtYXhEaXN0YW5jZSA9IHRoaXMubm9kZS54ICsgdGhpcy5nYW1lQ29udHJvbGxlci5ncmlkQXJlYS53aWR0aCoyO1xuICAgICAgICAgICAgdGhpcy5yb2NrZXRNb3ZlID0gd2luZG93LnNldEludGVydmFsKCgpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueCArPSAzO1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLm5vZGUueCA+IG1heERpc3RhbmNlKSB7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5jbGVhckludGVydmFsKHRoaXMucm9ja2V0TW92ZSk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChwb3BwZXJDaGlsZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcG9wcGVyQ2hpbGQuZ2V0Q29tcG9uZW50KFwicG9wcGVyXCIpLm9uUG9wcGVyVGFwcGVkKCk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkVycm9yLi4uXCIpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGVzdHJveSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sIDEwKTtcbiAgICAgICAgfSBlbHNlIGlmIChkaXJlY3Rpb24gPT0gMSkge1xuICAgICAgICAgICAgbGV0IG1heERpc3RhbmNlID0gdGhpcy5ub2RlLnkgKyB0aGlzLmdhbWVDb250cm9sbGVyLmdyaWRBcmVhLmhlaWdodCoyO1xuICAgICAgICAgICAgdGhpcy5yb2NrZXRNb3ZlID0gd2luZG93LnNldEludGVydmFsKCgpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueSArPSAzO1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLm5vZGUueSA+IG1heERpc3RhbmNlKSB7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5jbGVhckludGVydmFsKHRoaXMucm9ja2V0TW92ZSk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChwb3BwZXJDaGlsZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcG9wcGVyQ2hpbGQuZ2V0Q29tcG9uZW50KFwicG9wcGVyXCIpLm9uUG9wcGVyVGFwcGVkKCk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkVycm9yLi4uXCIpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGVzdHJveSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sIDEwKTtcbiAgICAgICAgfSBlbHNlIGlmIChkaXJlY3Rpb24gPT0gMikge1xuICAgICAgICAgICAgbGV0IG1heERpc3RhbmNlID0gdGhpcy5ub2RlLnggLSB0aGlzLmdhbWVDb250cm9sbGVyLmdyaWRBcmVhLndpZHRoKjI7XG4gICAgICAgICAgICB0aGlzLnJvY2tldE1vdmUgPSB3aW5kb3cuc2V0SW50ZXJ2YWwoKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMubm9kZS54IC09IDM7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMubm9kZS54IDwgbWF4RGlzdGFuY2UpIHtcbiAgICAgICAgICAgICAgICAgICAgd2luZG93LmNsZWFySW50ZXJ2YWwodGhpcy5yb2NrZXRNb3ZlKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHBvcHBlckNoaWxkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwb3BwZXJDaGlsZC5nZXRDb21wb25lbnQoXCJwb3BwZXJcIikub25Qb3BwZXJUYXBwZWQoKTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXJyb3IuLi5cIik7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kZXN0cm95KCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSwgMTApO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgbGV0IG1heERpc3RhbmNlID0gdGhpcy5ub2RlLnkgLSB0aGlzLmdhbWVDb250cm9sbGVyLmdyaWRBcmVhLmhlaWdodCoyO1xuICAgICAgICAgICAgdGhpcy5yb2NrZXRNb3ZlID0gd2luZG93LnNldEludGVydmFsKCgpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueSAtPSAzO1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLm5vZGUueSA8IG1heERpc3RhbmNlKSB7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5jbGVhckludGVydmFsKHRoaXMucm9ja2V0TW92ZSk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChwb3BwZXJDaGlsZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcG9wcGVyQ2hpbGQuZ2V0Q29tcG9uZW50KFwicG9wcGVyXCIpLm9uUG9wcGVyVGFwcGVkKCk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkVycm9yLi4uXCIpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGVzdHJveSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sIDEwKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG9uQ29sbGlzaW9uRW50ZXIgKG90aGVyOiBhbnksIHNlbGY6IGFueSkge1xuICAgICAgICBpZiAodGhpcy5yb2NrZXRUcmlnZ2VyZWQgJiYgb3RoZXIubm9kZS5nZXRDb21wb25lbnQoXCJwb3BwZXJcIikgJiYgIW90aGVyLm5vZGUuZ2V0Q29tcG9uZW50KFwicG9wcGVyXCIpLnBvcHBlckRlc3Ryb3llZCkge1xuICAgICAgICAgICAgb3RoZXIubm9kZS5nZXRDb21wb25lbnQoXCJwb3BwZXJcIikub25Qb3BwZXJUYXBwZWQoKTtcbiAgICAgICAgICAgIHdpbmRvdy5jbGVhckludGVydmFsKHRoaXMucm9ja2V0TW92ZSk7XG4gICAgICAgICAgICBzZWxmLm5vZGUuZGVzdHJveSgpO1xuICAgICAgICAgICAgLy8gdGhpcy5nYW1lQ29udHJvbGxlci5ncmlkSXRlbU1hbmFnZXIucmVtb3ZlR3JpZEl0ZW0odGhpcy5wb3BwZXJJRCk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgXG4gICAgb25Db2xsaXNpb25TdGF5IChvdGhlcjogYW55LCBzZWxmOiBhbnkpIHtcbiAgICB9XG4gICAgXG4gICAgb25Db2xsaXNpb25FeGl0IChvdGhlcjogYW55LCBzZWxmOiBhbnkpIHtcbiAgICAgICAgdGhpcy5yb2NrZXRUcmlnZ2VyZWQgPSB0cnVlO1xuICAgIH1cblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XG59XG4iXX0=