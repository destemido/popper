
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/gameDefinitions.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '467532uySFEhb+7DqGvXdJB', 'gameDefinitions');
// scripts/gameDefinitions.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GRID_STATE = void 0;
var GRID_STATE;
(function (GRID_STATE) {
    GRID_STATE[GRID_STATE["AVAILABLE"] = 0] = "AVAILABLE";
    GRID_STATE[GRID_STATE["FILLED"] = 1] = "FILLED";
})(GRID_STATE = exports.GRID_STATE || (exports.GRID_STATE = {}));

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL2dhbWVEZWZpbml0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFLQSxJQUFZLFVBR1g7QUFIRCxXQUFZLFVBQVU7SUFDbEIscURBQWEsQ0FBQTtJQUNiLCtDQUFVLENBQUE7QUFDZCxDQUFDLEVBSFcsVUFBVSxHQUFWLGtCQUFVLEtBQVYsa0JBQVUsUUFHckIiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIEdyaWRWYWx1ZSB7XG4gICAgcm93OiBudW1iZXI7XG4gICAgY29sdW1uOiBudW1iZXI7XG59XG5cbmV4cG9ydCBlbnVtIEdSSURfU1RBVEUge1xuICAgIEFWQUlMQUJMRSA9IDAsXG4gICAgRklMTEVEID0gMSxcbn0iXX0=