
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/gridItemManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'ae9a6OamCtJcbG08WXQNh/D', 'gridItemManager');
// scripts/gridItemManager.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var gameDefinitions_1 = require("./gameDefinitions");
var gridItem_1 = require("./gridItem");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var gridItemManager = /** @class */ (function (_super) {
    __extends(gridItemManager, _super);
    function gridItemManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.popperPrefab = null;
        _this.clap = null;
        _this.gameController = null;
        _this.grid = [];
        _this.gameOver = false;
        _this.testarray = [];
        return _this;
        // update (dt) {}
    }
    gridItemManager.prototype.onLoad = function () { };
    gridItemManager.prototype.start = function () {
    };
    gridItemManager.prototype.initializeGamePlay = function (Controller) {
        this.gameController = Controller;
        this.createGrid(1);
    };
    gridItemManager.prototype.createGrid = function (level) {
        this.gameOver = false;
        this.testarray = [];
        var testvar = [];
        if (level == 1) {
            this.addItemsToList();
        }
        else {
            this.resetGrid();
        }
        var gridSize = this.gameController.getGridDimention().column * this.gameController.getGridDimention().row;
        var itemsToSpawn = Math.min(level * 5, gridSize - 5) + Math.floor(Math.random() * 5) + 1;
        for (var i = 0; i < itemsToSpawn; i++) {
            var randomPos = Math.floor(Math.random() * gridSize);
            if (this.grid[randomPos].gridState == gameDefinitions_1.GRID_STATE.AVAILABLE) {
                this.grid[randomPos].gridState = gameDefinitions_1.GRID_STATE.FILLED;
                var popper = cc.instantiate(this.popperPrefab);
                popper.getComponent("popper").init(this.gameController, this.grid[randomPos].gridItemID);
                var currItem = new cc.Node();
                this.grid[randomPos].gridCurrItem = currItem;
                this.grid[randomPos].gridCurrItem.addChild(popper);
                this.grid[randomPos].gridCurrItem.setPosition(this.grid[randomPos].gridPosition);
                this.gameController.gridArea.addChild(this.grid[randomPos].gridCurrItem);
                testvar.push(randomPos);
                ;
            }
            else {
                i--;
            }
        }
        console.log("Items : ", testvar);
    };
    gridItemManager.prototype.addItemsToList = function () {
        var counter = 0;
        for (var row = 0; row < this.gameController.getGridDimention().row; row++) {
            for (var column = 0; column < this.gameController.getGridDimention().column; column++) {
                var newGridItem = new gridItem_1.default();
                newGridItem.gridItemID = counter;
                newGridItem.gridState = gameDefinitions_1.GRID_STATE.AVAILABLE;
                newGridItem.gridPosition = this.gameController.getGridPos()[counter];
                this.grid.push(newGridItem);
                counter++;
            }
        }
    };
    gridItemManager.prototype.removeGridItem = function (itemID) {
        this.grid[itemID].gridState = gameDefinitions_1.GRID_STATE.AVAILABLE;
        this.grid[itemID].gridCurrItem.removeAllChildren();
        if (!this.testarray.includes(itemID)) {
            this.testarray.push(itemID);
        }
        this.checkGameOver();
    };
    gridItemManager.prototype.resetGrid = function () {
        this.grid.forEach(function (item) {
            item.gridState = gameDefinitions_1.GRID_STATE.AVAILABLE;
            item.gridCurrItem = null;
        });
    };
    gridItemManager.prototype.checkGameOver = function () {
        var _this = this;
        var remaining = 0;
        var remaininggrid = 0;
        this.grid.forEach(function (item) {
            if (item.gridState == gameDefinitions_1.GRID_STATE.FILLED) {
                _this.gameOver = false;
                remaining++;
            }
        });
        if (remaining <= 3) {
            // this.grid.forEach(item => {
            //     if (item.gridCurrItem && item.gridCurrItem.children.length != 0) {
            //         remaininggrid++;
            //         if (remaininggrid == 1) {
            //             console.log(item);
            //         }
            //     }
            // });
            console.log("Issue have : ", this.testarray);
        }
        // console.log("Remaining : "+remaining+" : "+remaininggrid);
        if (remaining == 0 && this.gameOver == false) {
            console.log("STAGE COMPLETE");
            this.gameOver = true;
            // this.gameController.gridArea.removeAllChildren();
            cc.audioEngine.playEffect(this.clap, false);
            if (this.gameController.userLevel < 5) {
                this.gameController.showLevelUp();
            }
            else {
                this.gameController.showGameOver();
            }
        }
    };
    __decorate([
        property(cc.Prefab)
    ], gridItemManager.prototype, "popperPrefab", void 0);
    __decorate([
        property(cc.AudioClip)
    ], gridItemManager.prototype, "clap", void 0);
    gridItemManager = __decorate([
        ccclass
    ], gridItemManager);
    return gridItemManager;
}(cc.Component));
exports.default = gridItemManager;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL2dyaWRJdGVtTWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxxREFBK0M7QUFDL0MsdUNBQWtDO0FBRTVCLElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQTZDLG1DQUFZO0lBQXpEO1FBQUEscUVBdUhDO1FBcEhHLGtCQUFZLEdBQWMsSUFBSSxDQUFDO1FBRy9CLFVBQUksR0FBaUIsSUFBSSxDQUFDO1FBRWxCLG9CQUFjLEdBQW1CLElBQUksQ0FBQztRQUN2QyxVQUFJLEdBQW9CLEVBQUUsQ0FBQztRQUMxQixjQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLGVBQVMsR0FBa0IsRUFBRSxDQUFDOztRQTJHdEMsaUJBQWlCO0lBQ3JCLENBQUM7SUExR0csZ0NBQU0sR0FBTixjQUFXLENBQUM7SUFFWiwrQkFBSyxHQUFMO0lBRUEsQ0FBQztJQUVELDRDQUFrQixHQUFsQixVQUFtQixVQUEwQjtRQUN6QyxJQUFJLENBQUMsY0FBYyxHQUFHLFVBQVUsQ0FBQztRQUNqQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFFTSxvQ0FBVSxHQUFqQixVQUFrQixLQUFhO1FBQzNCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNqQixJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7WUFDWixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7U0FDekI7YUFBTTtZQUNILElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztTQUNwQjtRQUNELElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLEdBQUcsQ0FBQztRQUMxRyxJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUUsUUFBUSxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN6RixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsWUFBWSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ25DLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLFFBQVEsQ0FBQyxDQUFDO1lBQ3JELElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxTQUFTLElBQUksNEJBQVUsQ0FBQyxTQUFTLEVBQUU7Z0JBQ3hELElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUyxHQUFHLDRCQUFVLENBQUMsTUFBTSxDQUFDO2dCQUNuRCxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDL0MsTUFBTSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN6RixJQUFJLFFBQVEsR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDO2dCQUM3QyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ25ELElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUNqRixJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDekUsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFBQSxDQUFDO2FBQzVCO2lCQUFNO2dCQUNILENBQUMsRUFBRSxDQUFDO2FBQ1A7U0FDSjtRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFFTyx3Q0FBYyxHQUF0QjtRQUNJLElBQUksT0FBTyxHQUFHLENBQUMsQ0FBQztRQUNoQixLQUFLLElBQUksR0FBRyxHQUFHLENBQUMsRUFBRSxHQUFHLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsRUFBRTtZQUN2RSxLQUFLLElBQUksTUFBTSxHQUFHLENBQUMsRUFBRSxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLE1BQU0sRUFBRSxNQUFNLEVBQUUsRUFBRTtnQkFDbkYsSUFBSSxXQUFXLEdBQWEsSUFBSSxrQkFBUSxFQUFFLENBQUM7Z0JBQzNDLFdBQVcsQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDO2dCQUNqQyxXQUFXLENBQUMsU0FBUyxHQUFHLDRCQUFVLENBQUMsU0FBUyxDQUFDO2dCQUM3QyxXQUFXLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ3JFLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUM1QixPQUFPLEVBQUUsQ0FBQzthQUNiO1NBQ0o7SUFDTCxDQUFDO0lBRUQsd0NBQWMsR0FBZCxVQUFlLE1BQWM7UUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLEdBQUcsNEJBQVUsQ0FBQyxTQUFTLENBQUM7UUFDbkQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxZQUFZLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUNuRCxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDbEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDL0I7UUFDRCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDekIsQ0FBQztJQUVPLG1DQUFTLEdBQWpCO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO1lBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsNEJBQVUsQ0FBQyxTQUFTLENBQUM7WUFDdEMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDN0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU8sdUNBQWEsR0FBckI7UUFBQSxpQkFnQ0M7UUEvQkcsSUFBSSxTQUFTLEdBQUcsQ0FBQyxDQUFDO1FBQ2xCLElBQUksYUFBYSxHQUFHLENBQUMsQ0FBQztRQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7WUFDbEIsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLDRCQUFVLENBQUMsTUFBTSxFQUFFO2dCQUNyQyxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztnQkFDdEIsU0FBUyxFQUFFLENBQUM7YUFDZjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxTQUFTLElBQUksQ0FBQyxFQUFFO1lBQ2hCLDhCQUE4QjtZQUM5Qix5RUFBeUU7WUFDekUsMkJBQTJCO1lBQzNCLG9DQUFvQztZQUNwQyxpQ0FBaUM7WUFDakMsWUFBWTtZQUNaLFFBQVE7WUFDUixNQUFNO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQy9DO1FBQ0QsNkRBQTZEO1FBQzdELElBQUksU0FBUyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLEtBQUssRUFBRTtZQUMxQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDckIsb0RBQW9EO1lBQ3BELEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDNUMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsR0FBRyxDQUFDLEVBQUU7Z0JBQ25DLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFLENBQUM7YUFDckM7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQzthQUN0QztTQUNKO0lBQ0wsQ0FBQztJQWpIRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO3lEQUNXO0lBRy9CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUM7aURBQ0c7SUFOVCxlQUFlO1FBRG5DLE9BQU87T0FDYSxlQUFlLENBdUhuQztJQUFELHNCQUFDO0NBdkhELEFBdUhDLENBdkg0QyxFQUFFLENBQUMsU0FBUyxHQXVIeEQ7a0JBdkhvQixlQUFlIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGdhbWVDb250cm9sbGVyIGZyb20gXCIuL2dhbWVDb250cm9sbGVyXCI7XG5pbXBvcnQgeyBHUklEX1NUQVRFIH0gZnJvbSBcIi4vZ2FtZURlZmluaXRpb25zXCI7XG5pbXBvcnQgZ3JpZEl0ZW0gZnJvbSBcIi4vZ3JpZEl0ZW1cIjtcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBncmlkSXRlbU1hbmFnZXIgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXG4gICAgQHByb3BlcnR5KGNjLlByZWZhYilcbiAgICBwb3BwZXJQcmVmYWI6IGNjLlByZWZhYiA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuQXVkaW9DbGlwKVxuICAgIGNsYXA6IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XG5cbiAgICBwcml2YXRlIGdhbWVDb250cm9sbGVyOiBnYW1lQ29udHJvbGxlciA9IG51bGw7XG4gICAgcHVibGljIGdyaWQ6IEFycmF5PGdyaWRJdGVtPiA9IFtdO1xuICAgIHByaXZhdGUgZ2FtZU92ZXI6IGJvb2xlYW4gPSBmYWxzZTtcbiAgICBwcml2YXRlIHRlc3RhcnJheTogQXJyYXk8bnVtYmVyPiA9IFtdO1xuXG4gICAgb25Mb2FkICgpIHt9XG5cbiAgICBzdGFydCAoKSB7XG5cbiAgICB9XG5cbiAgICBpbml0aWFsaXplR2FtZVBsYXkoQ29udHJvbGxlcjogZ2FtZUNvbnRyb2xsZXIpIHtcbiAgICAgICAgdGhpcy5nYW1lQ29udHJvbGxlciA9IENvbnRyb2xsZXI7XG4gICAgICAgIHRoaXMuY3JlYXRlR3JpZCgxKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgY3JlYXRlR3JpZChsZXZlbDogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuZ2FtZU92ZXIgPSBmYWxzZTtcbiAgICAgICAgdGhpcy50ZXN0YXJyYXkgPSBbXTtcbiAgICAgICAgbGV0IHRlc3R2YXIgPSBbXTtcbiAgICAgICAgaWYgKGxldmVsID09IDEpIHtcbiAgICAgICAgICAgIHRoaXMuYWRkSXRlbXNUb0xpc3QoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMucmVzZXRHcmlkKCk7XG4gICAgICAgIH1cbiAgICAgICAgbGV0IGdyaWRTaXplID0gdGhpcy5nYW1lQ29udHJvbGxlci5nZXRHcmlkRGltZW50aW9uKCkuY29sdW1uICogdGhpcy5nYW1lQ29udHJvbGxlci5nZXRHcmlkRGltZW50aW9uKCkucm93O1xuICAgICAgICBsZXQgaXRlbXNUb1NwYXduID0gTWF0aC5taW4obGV2ZWwgKiA1LCBncmlkU2l6ZSAtIDUpICsgTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogNSkgKyAxO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGl0ZW1zVG9TcGF3bjsgaSsrKSB7XG4gICAgICAgICAgICBsZXQgcmFuZG9tUG9zID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogZ3JpZFNpemUpO1xuICAgICAgICAgICAgaWYgKHRoaXMuZ3JpZFtyYW5kb21Qb3NdLmdyaWRTdGF0ZSA9PSBHUklEX1NUQVRFLkFWQUlMQUJMRSkge1xuICAgICAgICAgICAgICAgIHRoaXMuZ3JpZFtyYW5kb21Qb3NdLmdyaWRTdGF0ZSA9IEdSSURfU1RBVEUuRklMTEVEO1xuICAgICAgICAgICAgICAgIGxldCBwb3BwZXIgPSBjYy5pbnN0YW50aWF0ZSh0aGlzLnBvcHBlclByZWZhYik7XG4gICAgICAgICAgICAgICAgcG9wcGVyLmdldENvbXBvbmVudChcInBvcHBlclwiKS5pbml0KHRoaXMuZ2FtZUNvbnRyb2xsZXIsIHRoaXMuZ3JpZFtyYW5kb21Qb3NdLmdyaWRJdGVtSUQpO1xuICAgICAgICAgICAgICAgIGxldCBjdXJySXRlbSA9IG5ldyBjYy5Ob2RlKCk7XG4gICAgICAgICAgICAgICAgdGhpcy5ncmlkW3JhbmRvbVBvc10uZ3JpZEN1cnJJdGVtID0gY3Vyckl0ZW07XG4gICAgICAgICAgICAgICAgdGhpcy5ncmlkW3JhbmRvbVBvc10uZ3JpZEN1cnJJdGVtLmFkZENoaWxkKHBvcHBlcik7XG4gICAgICAgICAgICAgICAgdGhpcy5ncmlkW3JhbmRvbVBvc10uZ3JpZEN1cnJJdGVtLnNldFBvc2l0aW9uKHRoaXMuZ3JpZFtyYW5kb21Qb3NdLmdyaWRQb3NpdGlvbik7XG4gICAgICAgICAgICAgICAgdGhpcy5nYW1lQ29udHJvbGxlci5ncmlkQXJlYS5hZGRDaGlsZCh0aGlzLmdyaWRbcmFuZG9tUG9zXS5ncmlkQ3Vyckl0ZW0pO1xuICAgICAgICAgICAgICAgIHRlc3R2YXIucHVzaChyYW5kb21Qb3MpOztcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgaS0tO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGNvbnNvbGUubG9nKFwiSXRlbXMgOiBcIiwgdGVzdHZhcik7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBhZGRJdGVtc1RvTGlzdCgpIHtcbiAgICAgICAgbGV0IGNvdW50ZXIgPSAwO1xuICAgICAgICBmb3IgKGxldCByb3cgPSAwOyByb3cgPCB0aGlzLmdhbWVDb250cm9sbGVyLmdldEdyaWREaW1lbnRpb24oKS5yb3c7IHJvdysrKSB7XG4gICAgICAgICAgICBmb3IgKGxldCBjb2x1bW4gPSAwOyBjb2x1bW4gPCB0aGlzLmdhbWVDb250cm9sbGVyLmdldEdyaWREaW1lbnRpb24oKS5jb2x1bW47IGNvbHVtbisrKSB7XG4gICAgICAgICAgICAgICAgdmFyIG5ld0dyaWRJdGVtOiBncmlkSXRlbSA9IG5ldyBncmlkSXRlbSgpO1xuICAgICAgICAgICAgICAgIG5ld0dyaWRJdGVtLmdyaWRJdGVtSUQgPSBjb3VudGVyO1xuICAgICAgICAgICAgICAgIG5ld0dyaWRJdGVtLmdyaWRTdGF0ZSA9IEdSSURfU1RBVEUuQVZBSUxBQkxFO1xuICAgICAgICAgICAgICAgIG5ld0dyaWRJdGVtLmdyaWRQb3NpdGlvbiA9IHRoaXMuZ2FtZUNvbnRyb2xsZXIuZ2V0R3JpZFBvcygpW2NvdW50ZXJdO1xuICAgICAgICAgICAgICAgIHRoaXMuZ3JpZC5wdXNoKG5ld0dyaWRJdGVtKTtcbiAgICAgICAgICAgICAgICBjb3VudGVyKys7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICByZW1vdmVHcmlkSXRlbShpdGVtSUQ6IG51bWJlcikge1xuICAgICAgICB0aGlzLmdyaWRbaXRlbUlEXS5ncmlkU3RhdGUgPSBHUklEX1NUQVRFLkFWQUlMQUJMRTtcbiAgICAgICAgdGhpcy5ncmlkW2l0ZW1JRF0uZ3JpZEN1cnJJdGVtLnJlbW92ZUFsbENoaWxkcmVuKCk7XG4gICAgICAgIGlmICghdGhpcy50ZXN0YXJyYXkuaW5jbHVkZXMoaXRlbUlEKSkge1xuICAgICAgICAgICAgdGhpcy50ZXN0YXJyYXkucHVzaChpdGVtSUQpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuY2hlY2tHYW1lT3ZlcigpO1xuICAgIH1cblxuICAgIHByaXZhdGUgcmVzZXRHcmlkKCkge1xuICAgICAgICB0aGlzLmdyaWQuZm9yRWFjaChpdGVtID0+IHtcbiAgICAgICAgICAgIGl0ZW0uZ3JpZFN0YXRlID0gR1JJRF9TVEFURS5BVkFJTEFCTEU7XG4gICAgICAgICAgICBpdGVtLmdyaWRDdXJySXRlbSA9IG51bGw7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHByaXZhdGUgY2hlY2tHYW1lT3ZlcigpIHtcbiAgICAgICAgbGV0IHJlbWFpbmluZyA9IDA7XG4gICAgICAgIGxldCByZW1haW5pbmdncmlkID0gMDtcbiAgICAgICAgdGhpcy5ncmlkLmZvckVhY2goaXRlbSA9PiB7XG4gICAgICAgICAgICBpZiAoaXRlbS5ncmlkU3RhdGUgPT0gR1JJRF9TVEFURS5GSUxMRUQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmdhbWVPdmVyID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgcmVtYWluaW5nKys7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICBpZiAocmVtYWluaW5nIDw9IDMpIHtcbiAgICAgICAgICAgIC8vIHRoaXMuZ3JpZC5mb3JFYWNoKGl0ZW0gPT4ge1xuICAgICAgICAgICAgLy8gICAgIGlmIChpdGVtLmdyaWRDdXJySXRlbSAmJiBpdGVtLmdyaWRDdXJySXRlbS5jaGlsZHJlbi5sZW5ndGggIT0gMCkge1xuICAgICAgICAgICAgLy8gICAgICAgICByZW1haW5pbmdncmlkKys7XG4gICAgICAgICAgICAvLyAgICAgICAgIGlmIChyZW1haW5pbmdncmlkID09IDEpIHtcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIGNvbnNvbGUubG9nKGl0ZW0pO1xuICAgICAgICAgICAgLy8gICAgICAgICB9XG4gICAgICAgICAgICAvLyAgICAgfVxuICAgICAgICAgICAgLy8gfSk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIklzc3VlIGhhdmUgOiBcIix0aGlzLnRlc3RhcnJheSk7XG4gICAgICAgIH1cbiAgICAgICAgLy8gY29uc29sZS5sb2coXCJSZW1haW5pbmcgOiBcIityZW1haW5pbmcrXCIgOiBcIityZW1haW5pbmdncmlkKTtcbiAgICAgICAgaWYgKHJlbWFpbmluZyA9PSAwICYmIHRoaXMuZ2FtZU92ZXIgPT0gZmFsc2UpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiU1RBR0UgQ09NUExFVEVcIik7XG4gICAgICAgICAgICB0aGlzLmdhbWVPdmVyID0gdHJ1ZTtcbiAgICAgICAgICAgIC8vIHRoaXMuZ2FtZUNvbnRyb2xsZXIuZ3JpZEFyZWEucmVtb3ZlQWxsQ2hpbGRyZW4oKTtcbiAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QodGhpcy5jbGFwLCBmYWxzZSk7XG4gICAgICAgICAgICBpZiAodGhpcy5nYW1lQ29udHJvbGxlci51c2VyTGV2ZWwgPCA1KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5nYW1lQ29udHJvbGxlci5zaG93TGV2ZWxVcCgpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLmdhbWVDb250cm9sbGVyLnNob3dHYW1lT3ZlcigpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy8gdXBkYXRlIChkdCkge31cbn1cbiJdfQ==