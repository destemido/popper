
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/gameController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'bce8c4j/pRE16TGUHmYYw3D', 'gameController');
// scripts/gameController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var gridItemManager_1 = require("./gridItemManager");
//app password : FAAprgF2rs2WKjH4j54p
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var gameController = /** @class */ (function (_super) {
    __extends(gameController, _super);
    function gameController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.rowSize = 5;
        _this.columnSize = 6;
        _this.gridItem = null;
        _this.gridArea = null;
        _this.gridItemManager = null;
        _this.levelUp = null;
        _this.gameOver = null;
        _this.level = null;
        _this.gridPos = [];
        _this.userLevel = 1;
        return _this;
        // update (dt) {}
    }
    gameController.prototype.onLoad = function () {
        this.createGrid();
    };
    gameController.prototype.start = function () {
        this.level.string = this.userLevel.toString();
        this.gridItemManager.initializeGamePlay(this);
    };
    gameController.prototype.createGrid = function () {
        this.gridArea.width = this.gridItem.width * this.columnSize;
        this.gridArea.height = this.gridItem.height * this.rowSize;
        var currItemPos = this.gridItem.getPosition();
        for (var i = 0; i < this.rowSize; i++) {
            for (var j = 0; j < this.columnSize; j++) {
                var itemPos = this.gridItem.getPosition();
                itemPos.x = currItemPos.x + j * this.gridItem.width;
                itemPos.y = currItemPos.y - i * this.gridItem.height;
                this.gridPos.push(itemPos);
            }
        }
    };
    gameController.prototype.getGridPos = function () {
        return this.gridPos;
    };
    gameController.prototype.getGridDimention = function () {
        return {
            row: this.rowSize,
            column: this.columnSize
        };
    };
    gameController.prototype.showLevelUp = function () {
        this.levelUp.runAction(cc.scaleTo(0.5, 1, 1));
    };
    gameController.prototype.showGameOver = function () {
        // this.gameOver.runAction(
        //     cc.sequence(
        //         cc.scaleTo(0.5, 1, 1),
        //         cc.scaleTo(0.5, 0, 0),
        //         cc.callFunc(() => {
        //             cc.director.loadScene("game");
        //         })
        //     )
        // );
        this.gameOver.runAction(cc.scaleTo(0.5, 1, 1));
    };
    gameController.prototype.onNextLevelClick = function () {
        this.levelUp.runAction(cc.sequence(cc.scaleTo(0.5, 0, 0), cc.callFunc(this.loadNextLevel.bind(this))));
    };
    gameController.prototype.loadNextLevel = function () {
        this.userLevel++;
        this.level.string = this.userLevel.toString();
        this.gridItemManager.createGrid(this.userLevel);
    };
    __decorate([
        property
    ], gameController.prototype, "rowSize", void 0);
    __decorate([
        property
    ], gameController.prototype, "columnSize", void 0);
    __decorate([
        property(cc.Node)
    ], gameController.prototype, "gridItem", void 0);
    __decorate([
        property(cc.Node)
    ], gameController.prototype, "gridArea", void 0);
    __decorate([
        property(gridItemManager_1.default)
    ], gameController.prototype, "gridItemManager", void 0);
    __decorate([
        property(cc.Node)
    ], gameController.prototype, "levelUp", void 0);
    __decorate([
        property(cc.Node)
    ], gameController.prototype, "gameOver", void 0);
    __decorate([
        property(cc.Label)
    ], gameController.prototype, "level", void 0);
    gameController = __decorate([
        ccclass
    ], gameController);
    return gameController;
}(cc.Component));
exports.default = gameController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL2dhbWVDb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHFEQUFnRDtBQUdoRCxxQ0FBcUM7QUFFL0IsSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFHMUM7SUFBNEMsa0NBQVk7SUFBeEQ7UUFBQSxxRUF1R0M7UUFwR0csYUFBTyxHQUFXLENBQUMsQ0FBQztRQUdwQixnQkFBVSxHQUFXLENBQUMsQ0FBQztRQUd2QixjQUFRLEdBQVksSUFBSSxDQUFDO1FBR3pCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFHekIscUJBQWUsR0FBb0IsSUFBSSxDQUFDO1FBR3hDLGFBQU8sR0FBWSxJQUFJLENBQUM7UUFHeEIsY0FBUSxHQUFZLElBQUksQ0FBQztRQUd6QixXQUFLLEdBQWEsSUFBSSxDQUFDO1FBRWYsYUFBTyxHQUFhLEVBQUUsQ0FBQztRQUN4QixlQUFTLEdBQVcsQ0FBQyxDQUFDOztRQTJFN0IsaUJBQWlCO0lBQ3JCLENBQUM7SUF6RUcsK0JBQU0sR0FBTjtRQUNJLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsOEJBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDOUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBRU8sbUNBQVUsR0FBbEI7UUFDSSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQzVELElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDM0QsSUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUVoRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNuQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDdEMsSUFBSSxPQUFPLEdBQVcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDbEQsT0FBTyxDQUFDLENBQUMsR0FBRyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQztnQkFDcEQsT0FBTyxDQUFDLENBQUMsR0FBRyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztnQkFDckQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDOUI7U0FDSjtJQUNMLENBQUM7SUFFRCxtQ0FBVSxHQUFWO1FBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3hCLENBQUM7SUFFRCx5Q0FBZ0IsR0FBaEI7UUFDSSxPQUFPO1lBQ0gsR0FBRyxFQUFFLElBQUksQ0FBQyxPQUFPO1lBQ2pCLE1BQU0sRUFBRSxJQUFJLENBQUMsVUFBVTtTQUMxQixDQUFBO0lBQ0wsQ0FBQztJQUVELG9DQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FDbEIsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUN4QixDQUFBO0lBQ0wsQ0FBQztJQUVELHFDQUFZLEdBQVo7UUFDSSwyQkFBMkI7UUFDM0IsbUJBQW1CO1FBQ25CLGlDQUFpQztRQUNqQyxpQ0FBaUM7UUFDakMsOEJBQThCO1FBQzlCLDZDQUE2QztRQUM3QyxhQUFhO1FBQ2IsUUFBUTtRQUNSLEtBQUs7UUFFTCxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FDbkIsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUN4QixDQUFDO0lBQ04sQ0FBQztJQUVELHlDQUFnQixHQUFoQjtRQUNJLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUNsQixFQUFFLENBQUMsUUFBUSxDQUNQLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFDckIsRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUM3QyxDQUNKLENBQUE7SUFDTCxDQUFDO0lBRUQsc0NBQWEsR0FBYjtRQUNJLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQzlDLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBakdEO1FBREMsUUFBUTttREFDVztJQUdwQjtRQURDLFFBQVE7c0RBQ2M7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztvREFDTztJQUd6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO29EQUNPO0lBR3pCO1FBREMsUUFBUSxDQUFDLHlCQUFlLENBQUM7MkRBQ2M7SUFHeEM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzttREFDTTtJQUd4QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO29EQUNPO0lBR3pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7aURBQ0k7SUF4Qk4sY0FBYztRQURsQyxPQUFPO09BQ2EsY0FBYyxDQXVHbEM7SUFBRCxxQkFBQztDQXZHRCxBQXVHQyxDQXZHMkMsRUFBRSxDQUFDLFNBQVMsR0F1R3ZEO2tCQXZHb0IsY0FBYyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBncmlkSXRlbU1hbmFnZXIgZnJvbSBcIi4vZ3JpZEl0ZW1NYW5hZ2VyXCI7XG5pbXBvcnQgeyBHcmlkVmFsdWUgfSBmcm9tIFwiLi9nYW1lRGVmaW5pdGlvbnNcIjtcblxuLy9hcHAgcGFzc3dvcmQgOiBGQUFwcmdGMnJzMldLakg0ajU0cFxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGdhbWVDb250cm9sbGVyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblxuICAgIEBwcm9wZXJ0eVxuICAgIHJvd1NpemU6IG51bWJlciA9IDU7XG5cbiAgICBAcHJvcGVydHlcbiAgICBjb2x1bW5TaXplOiBudW1iZXIgPSA2O1xuXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgZ3JpZEl0ZW06IGNjLk5vZGUgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgZ3JpZEFyZWE6IGNjLk5vZGUgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGdyaWRJdGVtTWFuYWdlcilcbiAgICBncmlkSXRlbU1hbmFnZXI6IGdyaWRJdGVtTWFuYWdlciA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICBsZXZlbFVwOiBjYy5Ob2RlID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICAgIGdhbWVPdmVyOiBjYy5Ob2RlID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICBsZXZlbDogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgcHJpdmF0ZSBncmlkUG9zOmNjLlZlYzJbXSA9IFtdO1xuICAgIHB1YmxpYyB1c2VyTGV2ZWw6IG51bWJlciA9IDE7XG5cblxuICAgIG9uTG9hZCAoKSB7XG4gICAgICAgIHRoaXMuY3JlYXRlR3JpZCgpO1xuICAgIH1cblxuICAgIHN0YXJ0ICgpIHtcbiAgICAgICAgdGhpcy5sZXZlbC5zdHJpbmcgPSB0aGlzLnVzZXJMZXZlbC50b1N0cmluZygpO1xuICAgICAgICB0aGlzLmdyaWRJdGVtTWFuYWdlci5pbml0aWFsaXplR2FtZVBsYXkodGhpcyk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBjcmVhdGVHcmlkKCkge1xuICAgICAgICB0aGlzLmdyaWRBcmVhLndpZHRoID0gdGhpcy5ncmlkSXRlbS53aWR0aCAqIHRoaXMuY29sdW1uU2l6ZTtcbiAgICAgICAgdGhpcy5ncmlkQXJlYS5oZWlnaHQgPSB0aGlzLmdyaWRJdGVtLmhlaWdodCAqIHRoaXMucm93U2l6ZTtcbiAgICAgICAgY29uc3QgY3Vyckl0ZW1Qb3MgPSB0aGlzLmdyaWRJdGVtLmdldFBvc2l0aW9uKCk7XG4gICAgICAgIFxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMucm93U2l6ZTsgaSsrKSB7XG4gICAgICAgICAgICBmb3IgKGxldCBqID0gMDsgaiA8IHRoaXMuY29sdW1uU2l6ZTsgaisrKSB7XG4gICAgICAgICAgICAgICAgbGV0IGl0ZW1Qb3M6Y2MuVmVjMiA9IHRoaXMuZ3JpZEl0ZW0uZ2V0UG9zaXRpb24oKTtcbiAgICAgICAgICAgICAgICBpdGVtUG9zLnggPSBjdXJySXRlbVBvcy54ICsgaiAqIHRoaXMuZ3JpZEl0ZW0ud2lkdGg7XG4gICAgICAgICAgICAgICAgaXRlbVBvcy55ID0gY3Vyckl0ZW1Qb3MueSAtIGkgKiB0aGlzLmdyaWRJdGVtLmhlaWdodDtcbiAgICAgICAgICAgICAgICB0aGlzLmdyaWRQb3MucHVzaChpdGVtUG9zKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIGdldEdyaWRQb3MoKTogY2MuVmVjMltdIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZ3JpZFBvcztcbiAgICB9XG5cbiAgICBnZXRHcmlkRGltZW50aW9uKCk6IEdyaWRWYWx1ZSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByb3c6IHRoaXMucm93U2l6ZSxcbiAgICAgICAgICAgIGNvbHVtbjogdGhpcy5jb2x1bW5TaXplXG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzaG93TGV2ZWxVcCgpIHtcbiAgICAgICAgdGhpcy5sZXZlbFVwLnJ1bkFjdGlvbihcbiAgICAgICAgICAgIGNjLnNjYWxlVG8oMC41LCAxLCAxKVxuICAgICAgICApXG4gICAgfVxuXG4gICAgc2hvd0dhbWVPdmVyKCkge1xuICAgICAgICAvLyB0aGlzLmdhbWVPdmVyLnJ1bkFjdGlvbihcbiAgICAgICAgLy8gICAgIGNjLnNlcXVlbmNlKFxuICAgICAgICAvLyAgICAgICAgIGNjLnNjYWxlVG8oMC41LCAxLCAxKSxcbiAgICAgICAgLy8gICAgICAgICBjYy5zY2FsZVRvKDAuNSwgMCwgMCksXG4gICAgICAgIC8vICAgICAgICAgY2MuY2FsbEZ1bmMoKCkgPT4ge1xuICAgICAgICAvLyAgICAgICAgICAgICBjYy5kaXJlY3Rvci5sb2FkU2NlbmUoXCJnYW1lXCIpO1xuICAgICAgICAvLyAgICAgICAgIH0pXG4gICAgICAgIC8vICAgICApXG4gICAgICAgIC8vICk7XG5cbiAgICAgICAgdGhpcy5nYW1lT3Zlci5ydW5BY3Rpb24oXG4gICAgICAgICAgICBjYy5zY2FsZVRvKDAuNSwgMSwgMSlcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBvbk5leHRMZXZlbENsaWNrKCkge1xuICAgICAgICB0aGlzLmxldmVsVXAucnVuQWN0aW9uKFxuICAgICAgICAgICAgY2Muc2VxdWVuY2UoXG4gICAgICAgICAgICAgICAgY2Muc2NhbGVUbygwLjUsIDAsIDApLFxuICAgICAgICAgICAgICAgIGNjLmNhbGxGdW5jKHRoaXMubG9hZE5leHRMZXZlbC5iaW5kKHRoaXMpKVxuICAgICAgICAgICAgKVxuICAgICAgICApXG4gICAgfVxuXG4gICAgbG9hZE5leHRMZXZlbCgpIHtcbiAgICAgICAgdGhpcy51c2VyTGV2ZWwrKztcbiAgICAgICAgdGhpcy5sZXZlbC5zdHJpbmcgPSB0aGlzLnVzZXJMZXZlbC50b1N0cmluZygpO1xuICAgICAgICB0aGlzLmdyaWRJdGVtTWFuYWdlci5jcmVhdGVHcmlkKHRoaXMudXNlckxldmVsKTtcbiAgICB9XG5cbiAgICAvLyB1cGRhdGUgKGR0KSB7fVxufVxuIl19