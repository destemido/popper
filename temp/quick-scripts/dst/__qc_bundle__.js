
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/__qc_index__.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}
require('./assets/scripts/collider');
require('./assets/scripts/gameController');
require('./assets/scripts/gameDefinitions');
require('./assets/scripts/gridItem');
require('./assets/scripts/gridItemManager');
require('./assets/scripts/popper');

                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/gridItemManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'ae9a6OamCtJcbG08WXQNh/D', 'gridItemManager');
// scripts/gridItemManager.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var gameDefinitions_1 = require("./gameDefinitions");
var gridItem_1 = require("./gridItem");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var gridItemManager = /** @class */ (function (_super) {
    __extends(gridItemManager, _super);
    function gridItemManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.popperPrefab = null;
        _this.clap = null;
        _this.gameController = null;
        _this.grid = [];
        _this.gameOver = false;
        _this.testarray = [];
        return _this;
        // update (dt) {}
    }
    gridItemManager.prototype.onLoad = function () { };
    gridItemManager.prototype.start = function () {
    };
    gridItemManager.prototype.initializeGamePlay = function (Controller) {
        this.gameController = Controller;
        this.createGrid(1);
    };
    gridItemManager.prototype.createGrid = function (level) {
        this.gameOver = false;
        this.testarray = [];
        var testvar = [];
        if (level == 1) {
            this.addItemsToList();
        }
        else {
            this.resetGrid();
        }
        var gridSize = this.gameController.getGridDimention().column * this.gameController.getGridDimention().row;
        var itemsToSpawn = Math.min(level * 5, gridSize - 5) + Math.floor(Math.random() * 5) + 1;
        for (var i = 0; i < itemsToSpawn; i++) {
            var randomPos = Math.floor(Math.random() * gridSize);
            if (this.grid[randomPos].gridState == gameDefinitions_1.GRID_STATE.AVAILABLE) {
                this.grid[randomPos].gridState = gameDefinitions_1.GRID_STATE.FILLED;
                var popper = cc.instantiate(this.popperPrefab);
                popper.getComponent("popper").init(this.gameController, this.grid[randomPos].gridItemID);
                var currItem = new cc.Node();
                this.grid[randomPos].gridCurrItem = currItem;
                this.grid[randomPos].gridCurrItem.addChild(popper);
                this.grid[randomPos].gridCurrItem.setPosition(this.grid[randomPos].gridPosition);
                this.gameController.gridArea.addChild(this.grid[randomPos].gridCurrItem);
                testvar.push(randomPos);
                ;
            }
            else {
                i--;
            }
        }
        console.log("Items : ", testvar);
    };
    gridItemManager.prototype.addItemsToList = function () {
        var counter = 0;
        for (var row = 0; row < this.gameController.getGridDimention().row; row++) {
            for (var column = 0; column < this.gameController.getGridDimention().column; column++) {
                var newGridItem = new gridItem_1.default();
                newGridItem.gridItemID = counter;
                newGridItem.gridState = gameDefinitions_1.GRID_STATE.AVAILABLE;
                newGridItem.gridPosition = this.gameController.getGridPos()[counter];
                this.grid.push(newGridItem);
                counter++;
            }
        }
    };
    gridItemManager.prototype.removeGridItem = function (itemID) {
        this.grid[itemID].gridState = gameDefinitions_1.GRID_STATE.AVAILABLE;
        this.grid[itemID].gridCurrItem.removeAllChildren();
        if (!this.testarray.includes(itemID)) {
            this.testarray.push(itemID);
        }
        this.checkGameOver();
    };
    gridItemManager.prototype.resetGrid = function () {
        this.grid.forEach(function (item) {
            item.gridState = gameDefinitions_1.GRID_STATE.AVAILABLE;
            item.gridCurrItem = null;
        });
    };
    gridItemManager.prototype.checkGameOver = function () {
        var _this = this;
        var remaining = 0;
        var remaininggrid = 0;
        this.grid.forEach(function (item) {
            if (item.gridState == gameDefinitions_1.GRID_STATE.FILLED) {
                _this.gameOver = false;
                remaining++;
            }
        });
        if (remaining <= 3) {
            // this.grid.forEach(item => {
            //     if (item.gridCurrItem && item.gridCurrItem.children.length != 0) {
            //         remaininggrid++;
            //         if (remaininggrid == 1) {
            //             console.log(item);
            //         }
            //     }
            // });
            console.log("Issue have : ", this.testarray);
        }
        // console.log("Remaining : "+remaining+" : "+remaininggrid);
        if (remaining == 0 && this.gameOver == false) {
            console.log("STAGE COMPLETE");
            this.gameOver = true;
            // this.gameController.gridArea.removeAllChildren();
            cc.audioEngine.playEffect(this.clap, false);
            if (this.gameController.userLevel < 5) {
                this.gameController.showLevelUp();
            }
            else {
                this.gameController.showGameOver();
            }
        }
    };
    __decorate([
        property(cc.Prefab)
    ], gridItemManager.prototype, "popperPrefab", void 0);
    __decorate([
        property(cc.AudioClip)
    ], gridItemManager.prototype, "clap", void 0);
    gridItemManager = __decorate([
        ccclass
    ], gridItemManager);
    return gridItemManager;
}(cc.Component));
exports.default = gridItemManager;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL2dyaWRJdGVtTWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSxxREFBK0M7QUFDL0MsdUNBQWtDO0FBRTVCLElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQTZDLG1DQUFZO0lBQXpEO1FBQUEscUVBdUhDO1FBcEhHLGtCQUFZLEdBQWMsSUFBSSxDQUFDO1FBRy9CLFVBQUksR0FBaUIsSUFBSSxDQUFDO1FBRWxCLG9CQUFjLEdBQW1CLElBQUksQ0FBQztRQUN2QyxVQUFJLEdBQW9CLEVBQUUsQ0FBQztRQUMxQixjQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLGVBQVMsR0FBa0IsRUFBRSxDQUFDOztRQTJHdEMsaUJBQWlCO0lBQ3JCLENBQUM7SUExR0csZ0NBQU0sR0FBTixjQUFXLENBQUM7SUFFWiwrQkFBSyxHQUFMO0lBRUEsQ0FBQztJQUVELDRDQUFrQixHQUFsQixVQUFtQixVQUEwQjtRQUN6QyxJQUFJLENBQUMsY0FBYyxHQUFHLFVBQVUsQ0FBQztRQUNqQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFFTSxvQ0FBVSxHQUFqQixVQUFrQixLQUFhO1FBQzNCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNqQixJQUFJLEtBQUssSUFBSSxDQUFDLEVBQUU7WUFDWixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7U0FDekI7YUFBTTtZQUNILElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztTQUNwQjtRQUNELElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLEdBQUcsQ0FBQztRQUMxRyxJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUUsUUFBUSxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN6RixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsWUFBWSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ25DLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLFFBQVEsQ0FBQyxDQUFDO1lBQ3JELElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxTQUFTLElBQUksNEJBQVUsQ0FBQyxTQUFTLEVBQUU7Z0JBQ3hELElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUyxHQUFHLDRCQUFVLENBQUMsTUFBTSxDQUFDO2dCQUNuRCxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDL0MsTUFBTSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN6RixJQUFJLFFBQVEsR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDO2dCQUM3QyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ25ELElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUNqRixJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDekUsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFBQSxDQUFDO2FBQzVCO2lCQUFNO2dCQUNILENBQUMsRUFBRSxDQUFDO2FBQ1A7U0FDSjtRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFFTyx3Q0FBYyxHQUF0QjtRQUNJLElBQUksT0FBTyxHQUFHLENBQUMsQ0FBQztRQUNoQixLQUFLLElBQUksR0FBRyxHQUFHLENBQUMsRUFBRSxHQUFHLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsRUFBRTtZQUN2RSxLQUFLLElBQUksTUFBTSxHQUFHLENBQUMsRUFBRSxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLE1BQU0sRUFBRSxNQUFNLEVBQUUsRUFBRTtnQkFDbkYsSUFBSSxXQUFXLEdBQWEsSUFBSSxrQkFBUSxFQUFFLENBQUM7Z0JBQzNDLFdBQVcsQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDO2dCQUNqQyxXQUFXLENBQUMsU0FBUyxHQUFHLDRCQUFVLENBQUMsU0FBUyxDQUFDO2dCQUM3QyxXQUFXLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ3JFLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUM1QixPQUFPLEVBQUUsQ0FBQzthQUNiO1NBQ0o7SUFDTCxDQUFDO0lBRUQsd0NBQWMsR0FBZCxVQUFlLE1BQWM7UUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLEdBQUcsNEJBQVUsQ0FBQyxTQUFTLENBQUM7UUFDbkQsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxZQUFZLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUNuRCxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDbEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDL0I7UUFDRCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDekIsQ0FBQztJQUVPLG1DQUFTLEdBQWpCO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO1lBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsNEJBQVUsQ0FBQyxTQUFTLENBQUM7WUFDdEMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDN0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU8sdUNBQWEsR0FBckI7UUFBQSxpQkFnQ0M7UUEvQkcsSUFBSSxTQUFTLEdBQUcsQ0FBQyxDQUFDO1FBQ2xCLElBQUksYUFBYSxHQUFHLENBQUMsQ0FBQztRQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7WUFDbEIsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLDRCQUFVLENBQUMsTUFBTSxFQUFFO2dCQUNyQyxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztnQkFDdEIsU0FBUyxFQUFFLENBQUM7YUFDZjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxTQUFTLElBQUksQ0FBQyxFQUFFO1lBQ2hCLDhCQUE4QjtZQUM5Qix5RUFBeUU7WUFDekUsMkJBQTJCO1lBQzNCLG9DQUFvQztZQUNwQyxpQ0FBaUM7WUFDakMsWUFBWTtZQUNaLFFBQVE7WUFDUixNQUFNO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQy9DO1FBQ0QsNkRBQTZEO1FBQzdELElBQUksU0FBUyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLEtBQUssRUFBRTtZQUMxQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDckIsb0RBQW9EO1lBQ3BELEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDNUMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsR0FBRyxDQUFDLEVBQUU7Z0JBQ25DLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFLENBQUM7YUFDckM7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQzthQUN0QztTQUNKO0lBQ0wsQ0FBQztJQWpIRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO3lEQUNXO0lBRy9CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUM7aURBQ0c7SUFOVCxlQUFlO1FBRG5DLE9BQU87T0FDYSxlQUFlLENBdUhuQztJQUFELHNCQUFDO0NBdkhELEFBdUhDLENBdkg0QyxFQUFFLENBQUMsU0FBUyxHQXVIeEQ7a0JBdkhvQixlQUFlIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGdhbWVDb250cm9sbGVyIGZyb20gXCIuL2dhbWVDb250cm9sbGVyXCI7XG5pbXBvcnQgeyBHUklEX1NUQVRFIH0gZnJvbSBcIi4vZ2FtZURlZmluaXRpb25zXCI7XG5pbXBvcnQgZ3JpZEl0ZW0gZnJvbSBcIi4vZ3JpZEl0ZW1cIjtcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBncmlkSXRlbU1hbmFnZXIgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xuXG4gICAgQHByb3BlcnR5KGNjLlByZWZhYilcbiAgICBwb3BwZXJQcmVmYWI6IGNjLlByZWZhYiA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuQXVkaW9DbGlwKVxuICAgIGNsYXA6IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XG5cbiAgICBwcml2YXRlIGdhbWVDb250cm9sbGVyOiBnYW1lQ29udHJvbGxlciA9IG51bGw7XG4gICAgcHVibGljIGdyaWQ6IEFycmF5PGdyaWRJdGVtPiA9IFtdO1xuICAgIHByaXZhdGUgZ2FtZU92ZXI6IGJvb2xlYW4gPSBmYWxzZTtcbiAgICBwcml2YXRlIHRlc3RhcnJheTogQXJyYXk8bnVtYmVyPiA9IFtdO1xuXG4gICAgb25Mb2FkICgpIHt9XG5cbiAgICBzdGFydCAoKSB7XG5cbiAgICB9XG5cbiAgICBpbml0aWFsaXplR2FtZVBsYXkoQ29udHJvbGxlcjogZ2FtZUNvbnRyb2xsZXIpIHtcbiAgICAgICAgdGhpcy5nYW1lQ29udHJvbGxlciA9IENvbnRyb2xsZXI7XG4gICAgICAgIHRoaXMuY3JlYXRlR3JpZCgxKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgY3JlYXRlR3JpZChsZXZlbDogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuZ2FtZU92ZXIgPSBmYWxzZTtcbiAgICAgICAgdGhpcy50ZXN0YXJyYXkgPSBbXTtcbiAgICAgICAgbGV0IHRlc3R2YXIgPSBbXTtcbiAgICAgICAgaWYgKGxldmVsID09IDEpIHtcbiAgICAgICAgICAgIHRoaXMuYWRkSXRlbXNUb0xpc3QoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMucmVzZXRHcmlkKCk7XG4gICAgICAgIH1cbiAgICAgICAgbGV0IGdyaWRTaXplID0gdGhpcy5nYW1lQ29udHJvbGxlci5nZXRHcmlkRGltZW50aW9uKCkuY29sdW1uICogdGhpcy5nYW1lQ29udHJvbGxlci5nZXRHcmlkRGltZW50aW9uKCkucm93O1xuICAgICAgICBsZXQgaXRlbXNUb1NwYXduID0gTWF0aC5taW4obGV2ZWwgKiA1LCBncmlkU2l6ZSAtIDUpICsgTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogNSkgKyAxO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGl0ZW1zVG9TcGF3bjsgaSsrKSB7XG4gICAgICAgICAgICBsZXQgcmFuZG9tUG9zID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogZ3JpZFNpemUpO1xuICAgICAgICAgICAgaWYgKHRoaXMuZ3JpZFtyYW5kb21Qb3NdLmdyaWRTdGF0ZSA9PSBHUklEX1NUQVRFLkFWQUlMQUJMRSkge1xuICAgICAgICAgICAgICAgIHRoaXMuZ3JpZFtyYW5kb21Qb3NdLmdyaWRTdGF0ZSA9IEdSSURfU1RBVEUuRklMTEVEO1xuICAgICAgICAgICAgICAgIGxldCBwb3BwZXIgPSBjYy5pbnN0YW50aWF0ZSh0aGlzLnBvcHBlclByZWZhYik7XG4gICAgICAgICAgICAgICAgcG9wcGVyLmdldENvbXBvbmVudChcInBvcHBlclwiKS5pbml0KHRoaXMuZ2FtZUNvbnRyb2xsZXIsIHRoaXMuZ3JpZFtyYW5kb21Qb3NdLmdyaWRJdGVtSUQpO1xuICAgICAgICAgICAgICAgIGxldCBjdXJySXRlbSA9IG5ldyBjYy5Ob2RlKCk7XG4gICAgICAgICAgICAgICAgdGhpcy5ncmlkW3JhbmRvbVBvc10uZ3JpZEN1cnJJdGVtID0gY3Vyckl0ZW07XG4gICAgICAgICAgICAgICAgdGhpcy5ncmlkW3JhbmRvbVBvc10uZ3JpZEN1cnJJdGVtLmFkZENoaWxkKHBvcHBlcik7XG4gICAgICAgICAgICAgICAgdGhpcy5ncmlkW3JhbmRvbVBvc10uZ3JpZEN1cnJJdGVtLnNldFBvc2l0aW9uKHRoaXMuZ3JpZFtyYW5kb21Qb3NdLmdyaWRQb3NpdGlvbik7XG4gICAgICAgICAgICAgICAgdGhpcy5nYW1lQ29udHJvbGxlci5ncmlkQXJlYS5hZGRDaGlsZCh0aGlzLmdyaWRbcmFuZG9tUG9zXS5ncmlkQ3Vyckl0ZW0pO1xuICAgICAgICAgICAgICAgIHRlc3R2YXIucHVzaChyYW5kb21Qb3MpOztcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgaS0tO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGNvbnNvbGUubG9nKFwiSXRlbXMgOiBcIiwgdGVzdHZhcik7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBhZGRJdGVtc1RvTGlzdCgpIHtcbiAgICAgICAgbGV0IGNvdW50ZXIgPSAwO1xuICAgICAgICBmb3IgKGxldCByb3cgPSAwOyByb3cgPCB0aGlzLmdhbWVDb250cm9sbGVyLmdldEdyaWREaW1lbnRpb24oKS5yb3c7IHJvdysrKSB7XG4gICAgICAgICAgICBmb3IgKGxldCBjb2x1bW4gPSAwOyBjb2x1bW4gPCB0aGlzLmdhbWVDb250cm9sbGVyLmdldEdyaWREaW1lbnRpb24oKS5jb2x1bW47IGNvbHVtbisrKSB7XG4gICAgICAgICAgICAgICAgdmFyIG5ld0dyaWRJdGVtOiBncmlkSXRlbSA9IG5ldyBncmlkSXRlbSgpO1xuICAgICAgICAgICAgICAgIG5ld0dyaWRJdGVtLmdyaWRJdGVtSUQgPSBjb3VudGVyO1xuICAgICAgICAgICAgICAgIG5ld0dyaWRJdGVtLmdyaWRTdGF0ZSA9IEdSSURfU1RBVEUuQVZBSUxBQkxFO1xuICAgICAgICAgICAgICAgIG5ld0dyaWRJdGVtLmdyaWRQb3NpdGlvbiA9IHRoaXMuZ2FtZUNvbnRyb2xsZXIuZ2V0R3JpZFBvcygpW2NvdW50ZXJdO1xuICAgICAgICAgICAgICAgIHRoaXMuZ3JpZC5wdXNoKG5ld0dyaWRJdGVtKTtcbiAgICAgICAgICAgICAgICBjb3VudGVyKys7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICByZW1vdmVHcmlkSXRlbShpdGVtSUQ6IG51bWJlcikge1xuICAgICAgICB0aGlzLmdyaWRbaXRlbUlEXS5ncmlkU3RhdGUgPSBHUklEX1NUQVRFLkFWQUlMQUJMRTtcbiAgICAgICAgdGhpcy5ncmlkW2l0ZW1JRF0uZ3JpZEN1cnJJdGVtLnJlbW92ZUFsbENoaWxkcmVuKCk7XG4gICAgICAgIGlmICghdGhpcy50ZXN0YXJyYXkuaW5jbHVkZXMoaXRlbUlEKSkge1xuICAgICAgICAgICAgdGhpcy50ZXN0YXJyYXkucHVzaChpdGVtSUQpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuY2hlY2tHYW1lT3ZlcigpO1xuICAgIH1cblxuICAgIHByaXZhdGUgcmVzZXRHcmlkKCkge1xuICAgICAgICB0aGlzLmdyaWQuZm9yRWFjaChpdGVtID0+IHtcbiAgICAgICAgICAgIGl0ZW0uZ3JpZFN0YXRlID0gR1JJRF9TVEFURS5BVkFJTEFCTEU7XG4gICAgICAgICAgICBpdGVtLmdyaWRDdXJySXRlbSA9IG51bGw7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHByaXZhdGUgY2hlY2tHYW1lT3ZlcigpIHtcbiAgICAgICAgbGV0IHJlbWFpbmluZyA9IDA7XG4gICAgICAgIGxldCByZW1haW5pbmdncmlkID0gMDtcbiAgICAgICAgdGhpcy5ncmlkLmZvckVhY2goaXRlbSA9PiB7XG4gICAgICAgICAgICBpZiAoaXRlbS5ncmlkU3RhdGUgPT0gR1JJRF9TVEFURS5GSUxMRUQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmdhbWVPdmVyID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgcmVtYWluaW5nKys7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICBpZiAocmVtYWluaW5nIDw9IDMpIHtcbiAgICAgICAgICAgIC8vIHRoaXMuZ3JpZC5mb3JFYWNoKGl0ZW0gPT4ge1xuICAgICAgICAgICAgLy8gICAgIGlmIChpdGVtLmdyaWRDdXJySXRlbSAmJiBpdGVtLmdyaWRDdXJySXRlbS5jaGlsZHJlbi5sZW5ndGggIT0gMCkge1xuICAgICAgICAgICAgLy8gICAgICAgICByZW1haW5pbmdncmlkKys7XG4gICAgICAgICAgICAvLyAgICAgICAgIGlmIChyZW1haW5pbmdncmlkID09IDEpIHtcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIGNvbnNvbGUubG9nKGl0ZW0pO1xuICAgICAgICAgICAgLy8gICAgICAgICB9XG4gICAgICAgICAgICAvLyAgICAgfVxuICAgICAgICAgICAgLy8gfSk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIklzc3VlIGhhdmUgOiBcIix0aGlzLnRlc3RhcnJheSk7XG4gICAgICAgIH1cbiAgICAgICAgLy8gY29uc29sZS5sb2coXCJSZW1haW5pbmcgOiBcIityZW1haW5pbmcrXCIgOiBcIityZW1haW5pbmdncmlkKTtcbiAgICAgICAgaWYgKHJlbWFpbmluZyA9PSAwICYmIHRoaXMuZ2FtZU92ZXIgPT0gZmFsc2UpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiU1RBR0UgQ09NUExFVEVcIik7XG4gICAgICAgICAgICB0aGlzLmdhbWVPdmVyID0gdHJ1ZTtcbiAgICAgICAgICAgIC8vIHRoaXMuZ2FtZUNvbnRyb2xsZXIuZ3JpZEFyZWEucmVtb3ZlQWxsQ2hpbGRyZW4oKTtcbiAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QodGhpcy5jbGFwLCBmYWxzZSk7XG4gICAgICAgICAgICBpZiAodGhpcy5nYW1lQ29udHJvbGxlci51c2VyTGV2ZWwgPCA1KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5nYW1lQ29udHJvbGxlci5zaG93TGV2ZWxVcCgpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLmdhbWVDb250cm9sbGVyLnNob3dHYW1lT3ZlcigpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy8gdXBkYXRlIChkdCkge31cbn1cbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/gridItem.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '1bd34ZgOsJAC7KVvug/iy+A', 'gridItem');
// scripts/gridItem.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var gridItem = /** @class */ (function (_super) {
    __extends(gridItem, _super);
    function gridItem() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    gridItem.prototype.onLoad = function () { };
    gridItem.prototype.start = function () {
    };
    gridItem = __decorate([
        ccclass
    ], gridItem);
    return gridItem;
}(cc.Component));
exports.default = gridItem;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL2dyaWRJdGVtLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVNLElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQXNDLDRCQUFZO0lBQWxEOztJQWNBLENBQUM7SUFQRyx5QkFBTSxHQUFOLGNBQVcsQ0FBQztJQUVaLHdCQUFLLEdBQUw7SUFFQSxDQUFDO0lBWGdCLFFBQVE7UUFENUIsT0FBTztPQUNhLFFBQVEsQ0FjNUI7SUFBRCxlQUFDO0NBZEQsQUFjQyxDQWRxQyxFQUFFLENBQUMsU0FBUyxHQWNqRDtrQkFkb0IsUUFBUSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEdSSURfU1RBVEUgfSBmcm9tIFwiLi9nYW1lRGVmaW5pdGlvbnNcIjtcblxuY29uc3Qge2NjY2xhc3MsIHByb3BlcnR5fSA9IGNjLl9kZWNvcmF0b3I7XG5cbkBjY2NsYXNzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBncmlkSXRlbSBleHRlbmRzIGNjLkNvbXBvbmVudCB7XG5cbiAgICBwdWJsaWMgZ3JpZEl0ZW1JRDogbnVtYmVyO1xuICAgIHB1YmxpYyBncmlkUG9zaXRpb246IGNjLlZlYzI7XG4gICAgcHVibGljIGdyaWRTdGF0ZTogR1JJRF9TVEFURTtcbiAgICBwdWJsaWMgZ3JpZEN1cnJJdGVtOiBjYy5Ob2RlO1xuXG4gICAgb25Mb2FkICgpIHt9XG5cbiAgICBzdGFydCAoKSB7XG5cbiAgICB9XG5cbiAgICAvLyB1cGRhdGUgKGR0KSB7fVxufVxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/gameController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'bce8c4j/pRE16TGUHmYYw3D', 'gameController');
// scripts/gameController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var gridItemManager_1 = require("./gridItemManager");
//app password : FAAprgF2rs2WKjH4j54p
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var gameController = /** @class */ (function (_super) {
    __extends(gameController, _super);
    function gameController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.rowSize = 5;
        _this.columnSize = 6;
        _this.gridItem = null;
        _this.gridArea = null;
        _this.gridItemManager = null;
        _this.levelUp = null;
        _this.gameOver = null;
        _this.level = null;
        _this.gridPos = [];
        _this.userLevel = 1;
        return _this;
        // update (dt) {}
    }
    gameController.prototype.onLoad = function () {
        this.createGrid();
    };
    gameController.prototype.start = function () {
        this.level.string = this.userLevel.toString();
        this.gridItemManager.initializeGamePlay(this);
    };
    gameController.prototype.createGrid = function () {
        this.gridArea.width = this.gridItem.width * this.columnSize;
        this.gridArea.height = this.gridItem.height * this.rowSize;
        var currItemPos = this.gridItem.getPosition();
        for (var i = 0; i < this.rowSize; i++) {
            for (var j = 0; j < this.columnSize; j++) {
                var itemPos = this.gridItem.getPosition();
                itemPos.x = currItemPos.x + j * this.gridItem.width;
                itemPos.y = currItemPos.y - i * this.gridItem.height;
                this.gridPos.push(itemPos);
            }
        }
    };
    gameController.prototype.getGridPos = function () {
        return this.gridPos;
    };
    gameController.prototype.getGridDimention = function () {
        return {
            row: this.rowSize,
            column: this.columnSize
        };
    };
    gameController.prototype.showLevelUp = function () {
        this.levelUp.runAction(cc.scaleTo(0.5, 1, 1));
    };
    gameController.prototype.showGameOver = function () {
        // this.gameOver.runAction(
        //     cc.sequence(
        //         cc.scaleTo(0.5, 1, 1),
        //         cc.scaleTo(0.5, 0, 0),
        //         cc.callFunc(() => {
        //             cc.director.loadScene("game");
        //         })
        //     )
        // );
        this.gameOver.runAction(cc.scaleTo(0.5, 1, 1));
    };
    gameController.prototype.onNextLevelClick = function () {
        this.levelUp.runAction(cc.sequence(cc.scaleTo(0.5, 0, 0), cc.callFunc(this.loadNextLevel.bind(this))));
    };
    gameController.prototype.loadNextLevel = function () {
        this.userLevel++;
        this.level.string = this.userLevel.toString();
        this.gridItemManager.createGrid(this.userLevel);
    };
    __decorate([
        property
    ], gameController.prototype, "rowSize", void 0);
    __decorate([
        property
    ], gameController.prototype, "columnSize", void 0);
    __decorate([
        property(cc.Node)
    ], gameController.prototype, "gridItem", void 0);
    __decorate([
        property(cc.Node)
    ], gameController.prototype, "gridArea", void 0);
    __decorate([
        property(gridItemManager_1.default)
    ], gameController.prototype, "gridItemManager", void 0);
    __decorate([
        property(cc.Node)
    ], gameController.prototype, "levelUp", void 0);
    __decorate([
        property(cc.Node)
    ], gameController.prototype, "gameOver", void 0);
    __decorate([
        property(cc.Label)
    ], gameController.prototype, "level", void 0);
    gameController = __decorate([
        ccclass
    ], gameController);
    return gameController;
}(cc.Component));
exports.default = gameController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL2dhbWVDb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHFEQUFnRDtBQUdoRCxxQ0FBcUM7QUFFL0IsSUFBQSxLQUFzQixFQUFFLENBQUMsVUFBVSxFQUFsQyxPQUFPLGFBQUEsRUFBRSxRQUFRLGNBQWlCLENBQUM7QUFHMUM7SUFBNEMsa0NBQVk7SUFBeEQ7UUFBQSxxRUF1R0M7UUFwR0csYUFBTyxHQUFXLENBQUMsQ0FBQztRQUdwQixnQkFBVSxHQUFXLENBQUMsQ0FBQztRQUd2QixjQUFRLEdBQVksSUFBSSxDQUFDO1FBR3pCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFHekIscUJBQWUsR0FBb0IsSUFBSSxDQUFDO1FBR3hDLGFBQU8sR0FBWSxJQUFJLENBQUM7UUFHeEIsY0FBUSxHQUFZLElBQUksQ0FBQztRQUd6QixXQUFLLEdBQWEsSUFBSSxDQUFDO1FBRWYsYUFBTyxHQUFhLEVBQUUsQ0FBQztRQUN4QixlQUFTLEdBQVcsQ0FBQyxDQUFDOztRQTJFN0IsaUJBQWlCO0lBQ3JCLENBQUM7SUF6RUcsK0JBQU0sR0FBTjtRQUNJLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsOEJBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDOUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBRU8sbUNBQVUsR0FBbEI7UUFDSSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQzVELElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDM0QsSUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUVoRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNuQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDdEMsSUFBSSxPQUFPLEdBQVcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDbEQsT0FBTyxDQUFDLENBQUMsR0FBRyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQztnQkFDcEQsT0FBTyxDQUFDLENBQUMsR0FBRyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztnQkFDckQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDOUI7U0FDSjtJQUNMLENBQUM7SUFFRCxtQ0FBVSxHQUFWO1FBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3hCLENBQUM7SUFFRCx5Q0FBZ0IsR0FBaEI7UUFDSSxPQUFPO1lBQ0gsR0FBRyxFQUFFLElBQUksQ0FBQyxPQUFPO1lBQ2pCLE1BQU0sRUFBRSxJQUFJLENBQUMsVUFBVTtTQUMxQixDQUFBO0lBQ0wsQ0FBQztJQUVELG9DQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FDbEIsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUN4QixDQUFBO0lBQ0wsQ0FBQztJQUVELHFDQUFZLEdBQVo7UUFDSSwyQkFBMkI7UUFDM0IsbUJBQW1CO1FBQ25CLGlDQUFpQztRQUNqQyxpQ0FBaUM7UUFDakMsOEJBQThCO1FBQzlCLDZDQUE2QztRQUM3QyxhQUFhO1FBQ2IsUUFBUTtRQUNSLEtBQUs7UUFFTCxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FDbkIsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUN4QixDQUFDO0lBQ04sQ0FBQztJQUVELHlDQUFnQixHQUFoQjtRQUNJLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUNsQixFQUFFLENBQUMsUUFBUSxDQUNQLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFDckIsRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUM3QyxDQUNKLENBQUE7SUFDTCxDQUFDO0lBRUQsc0NBQWEsR0FBYjtRQUNJLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQzlDLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBakdEO1FBREMsUUFBUTttREFDVztJQUdwQjtRQURDLFFBQVE7c0RBQ2M7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztvREFDTztJQUd6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO29EQUNPO0lBR3pCO1FBREMsUUFBUSxDQUFDLHlCQUFlLENBQUM7MkRBQ2M7SUFHeEM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzttREFDTTtJQUd4QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO29EQUNPO0lBR3pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7aURBQ0k7SUF4Qk4sY0FBYztRQURsQyxPQUFPO09BQ2EsY0FBYyxDQXVHbEM7SUFBRCxxQkFBQztDQXZHRCxBQXVHQyxDQXZHMkMsRUFBRSxDQUFDLFNBQVMsR0F1R3ZEO2tCQXZHb0IsY0FBYyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBncmlkSXRlbU1hbmFnZXIgZnJvbSBcIi4vZ3JpZEl0ZW1NYW5hZ2VyXCI7XG5pbXBvcnQgeyBHcmlkVmFsdWUgfSBmcm9tIFwiLi9nYW1lRGVmaW5pdGlvbnNcIjtcblxuLy9hcHAgcGFzc3dvcmQgOiBGQUFwcmdGMnJzMldLakg0ajU0cFxuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGdhbWVDb250cm9sbGVyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblxuICAgIEBwcm9wZXJ0eVxuICAgIHJvd1NpemU6IG51bWJlciA9IDU7XG5cbiAgICBAcHJvcGVydHlcbiAgICBjb2x1bW5TaXplOiBudW1iZXIgPSA2O1xuXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgZ3JpZEl0ZW06IGNjLk5vZGUgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgZ3JpZEFyZWE6IGNjLk5vZGUgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGdyaWRJdGVtTWFuYWdlcilcbiAgICBncmlkSXRlbU1hbmFnZXI6IGdyaWRJdGVtTWFuYWdlciA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICBsZXZlbFVwOiBjYy5Ob2RlID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxuICAgIGdhbWVPdmVyOiBjYy5Ob2RlID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5MYWJlbClcbiAgICBsZXZlbDogY2MuTGFiZWwgPSBudWxsO1xuXG4gICAgcHJpdmF0ZSBncmlkUG9zOmNjLlZlYzJbXSA9IFtdO1xuICAgIHB1YmxpYyB1c2VyTGV2ZWw6IG51bWJlciA9IDE7XG5cblxuICAgIG9uTG9hZCAoKSB7XG4gICAgICAgIHRoaXMuY3JlYXRlR3JpZCgpO1xuICAgIH1cblxuICAgIHN0YXJ0ICgpIHtcbiAgICAgICAgdGhpcy5sZXZlbC5zdHJpbmcgPSB0aGlzLnVzZXJMZXZlbC50b1N0cmluZygpO1xuICAgICAgICB0aGlzLmdyaWRJdGVtTWFuYWdlci5pbml0aWFsaXplR2FtZVBsYXkodGhpcyk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBjcmVhdGVHcmlkKCkge1xuICAgICAgICB0aGlzLmdyaWRBcmVhLndpZHRoID0gdGhpcy5ncmlkSXRlbS53aWR0aCAqIHRoaXMuY29sdW1uU2l6ZTtcbiAgICAgICAgdGhpcy5ncmlkQXJlYS5oZWlnaHQgPSB0aGlzLmdyaWRJdGVtLmhlaWdodCAqIHRoaXMucm93U2l6ZTtcbiAgICAgICAgY29uc3QgY3Vyckl0ZW1Qb3MgPSB0aGlzLmdyaWRJdGVtLmdldFBvc2l0aW9uKCk7XG4gICAgICAgIFxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMucm93U2l6ZTsgaSsrKSB7XG4gICAgICAgICAgICBmb3IgKGxldCBqID0gMDsgaiA8IHRoaXMuY29sdW1uU2l6ZTsgaisrKSB7XG4gICAgICAgICAgICAgICAgbGV0IGl0ZW1Qb3M6Y2MuVmVjMiA9IHRoaXMuZ3JpZEl0ZW0uZ2V0UG9zaXRpb24oKTtcbiAgICAgICAgICAgICAgICBpdGVtUG9zLnggPSBjdXJySXRlbVBvcy54ICsgaiAqIHRoaXMuZ3JpZEl0ZW0ud2lkdGg7XG4gICAgICAgICAgICAgICAgaXRlbVBvcy55ID0gY3Vyckl0ZW1Qb3MueSAtIGkgKiB0aGlzLmdyaWRJdGVtLmhlaWdodDtcbiAgICAgICAgICAgICAgICB0aGlzLmdyaWRQb3MucHVzaChpdGVtUG9zKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIGdldEdyaWRQb3MoKTogY2MuVmVjMltdIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZ3JpZFBvcztcbiAgICB9XG5cbiAgICBnZXRHcmlkRGltZW50aW9uKCk6IEdyaWRWYWx1ZSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByb3c6IHRoaXMucm93U2l6ZSxcbiAgICAgICAgICAgIGNvbHVtbjogdGhpcy5jb2x1bW5TaXplXG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzaG93TGV2ZWxVcCgpIHtcbiAgICAgICAgdGhpcy5sZXZlbFVwLnJ1bkFjdGlvbihcbiAgICAgICAgICAgIGNjLnNjYWxlVG8oMC41LCAxLCAxKVxuICAgICAgICApXG4gICAgfVxuXG4gICAgc2hvd0dhbWVPdmVyKCkge1xuICAgICAgICAvLyB0aGlzLmdhbWVPdmVyLnJ1bkFjdGlvbihcbiAgICAgICAgLy8gICAgIGNjLnNlcXVlbmNlKFxuICAgICAgICAvLyAgICAgICAgIGNjLnNjYWxlVG8oMC41LCAxLCAxKSxcbiAgICAgICAgLy8gICAgICAgICBjYy5zY2FsZVRvKDAuNSwgMCwgMCksXG4gICAgICAgIC8vICAgICAgICAgY2MuY2FsbEZ1bmMoKCkgPT4ge1xuICAgICAgICAvLyAgICAgICAgICAgICBjYy5kaXJlY3Rvci5sb2FkU2NlbmUoXCJnYW1lXCIpO1xuICAgICAgICAvLyAgICAgICAgIH0pXG4gICAgICAgIC8vICAgICApXG4gICAgICAgIC8vICk7XG5cbiAgICAgICAgdGhpcy5nYW1lT3Zlci5ydW5BY3Rpb24oXG4gICAgICAgICAgICBjYy5zY2FsZVRvKDAuNSwgMSwgMSlcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBvbk5leHRMZXZlbENsaWNrKCkge1xuICAgICAgICB0aGlzLmxldmVsVXAucnVuQWN0aW9uKFxuICAgICAgICAgICAgY2Muc2VxdWVuY2UoXG4gICAgICAgICAgICAgICAgY2Muc2NhbGVUbygwLjUsIDAsIDApLFxuICAgICAgICAgICAgICAgIGNjLmNhbGxGdW5jKHRoaXMubG9hZE5leHRMZXZlbC5iaW5kKHRoaXMpKVxuICAgICAgICAgICAgKVxuICAgICAgICApXG4gICAgfVxuXG4gICAgbG9hZE5leHRMZXZlbCgpIHtcbiAgICAgICAgdGhpcy51c2VyTGV2ZWwrKztcbiAgICAgICAgdGhpcy5sZXZlbC5zdHJpbmcgPSB0aGlzLnVzZXJMZXZlbC50b1N0cmluZygpO1xuICAgICAgICB0aGlzLmdyaWRJdGVtTWFuYWdlci5jcmVhdGVHcmlkKHRoaXMudXNlckxldmVsKTtcbiAgICB9XG5cbiAgICAvLyB1cGRhdGUgKGR0KSB7fVxufVxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/gameDefinitions.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '467532uySFEhb+7DqGvXdJB', 'gameDefinitions');
// scripts/gameDefinitions.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GRID_STATE = void 0;
var GRID_STATE;
(function (GRID_STATE) {
    GRID_STATE[GRID_STATE["AVAILABLE"] = 0] = "AVAILABLE";
    GRID_STATE[GRID_STATE["FILLED"] = 1] = "FILLED";
})(GRID_STATE = exports.GRID_STATE || (exports.GRID_STATE = {}));

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL2dhbWVEZWZpbml0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFLQSxJQUFZLFVBR1g7QUFIRCxXQUFZLFVBQVU7SUFDbEIscURBQWEsQ0FBQTtJQUNiLCtDQUFVLENBQUE7QUFDZCxDQUFDLEVBSFcsVUFBVSxHQUFWLGtCQUFVLEtBQVYsa0JBQVUsUUFHckIiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIEdyaWRWYWx1ZSB7XG4gICAgcm93OiBudW1iZXI7XG4gICAgY29sdW1uOiBudW1iZXI7XG59XG5cbmV4cG9ydCBlbnVtIEdSSURfU1RBVEUge1xuICAgIEFWQUlMQUJMRSA9IDAsXG4gICAgRklMTEVEID0gMSxcbn0iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/popper.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '997ecnFPMNNBaOcj5hdKsKF', 'popper');
// scripts/popper.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var popper = /** @class */ (function (_super) {
    __extends(popper, _super);
    function popper() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.popperLevel = 2;
        _this.popperID = null;
        _this.gameController = null;
        _this.popperDestroyed = false;
        _this.popperSprite = null;
        _this.popperExplod = null;
        _this.popperEye = null;
        _this.popperVarients = [];
        _this.rocket = null;
        _this.popperBtn = null;
        _this.popperTap = null;
        return _this;
        // update (dt) {}
    }
    popper.prototype.onLoad = function () { };
    popper.prototype.init = function (controller, itemID) {
        this.gameController = controller;
        this.popperID = itemID;
        this.popperLevel = Math.floor(Math.random() * this.popperVarients.length);
    };
    popper.prototype.start = function () {
        this.popperSprite.spriteFrame = this.popperVarients[this.popperLevel];
    };
    popper.prototype.onPopperTapped = function () {
        if (this.popperLevel > 0) {
            cc.audioEngine.playEffect(this.popperTap, false);
            this.popperLevel--;
            this.popperSprite.spriteFrame = this.popperVarients[this.popperLevel];
        }
        else if (!this.popperDestroyed) {
            cc.audioEngine.playEffect(this.popperTap, false);
            this.popperBtn.interactable = false;
            this.popperDestroyed = true;
            this.popperExplod.runAction(cc.sequence(cc.scaleTo(0.5, 1, 1), cc.callFunc(this.hidePopper.bind(this)), cc.scaleTo(0.5, 0, 0), cc.callFunc(this.onPopperDestroyCallback.bind(this))));
        }
        else {
            this.gameController.gridItemManager.removeGridItem(this.popperID);
        }
    };
    popper.prototype.hidePopper = function () {
        this.popperSprite.node.active = false;
        this.popperEye.active = false;
    };
    popper.prototype.onPopperDestroyCallback = function () {
        for (var i = 0; i < 4; i++) {
            var rocket = cc.instantiate(this.rocket);
            rocket.getComponent("collider").init(i, this.popperID, this.gameController);
            this.node.addChild(rocket);
        }
    };
    __decorate([
        property(cc.Sprite)
    ], popper.prototype, "popperSprite", void 0);
    __decorate([
        property(cc.Node)
    ], popper.prototype, "popperExplod", void 0);
    __decorate([
        property(cc.Node)
    ], popper.prototype, "popperEye", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], popper.prototype, "popperVarients", void 0);
    __decorate([
        property(cc.Prefab)
    ], popper.prototype, "rocket", void 0);
    __decorate([
        property(cc.Button)
    ], popper.prototype, "popperBtn", void 0);
    __decorate([
        property(cc.AudioClip)
    ], popper.prototype, "popperTap", void 0);
    popper = __decorate([
        ccclass
    ], popper);
    return popper;
}(cc.Component));
exports.default = popper;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL3BvcHBlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFTSxJQUFBLEtBQXNCLEVBQUUsQ0FBQyxVQUFVLEVBQWxDLE9BQU8sYUFBQSxFQUFFLFFBQVEsY0FBaUIsQ0FBQztBQUcxQztJQUFvQywwQkFBWTtJQUFoRDtRQUFBLHFFQTRFQztRQTFFVyxpQkFBVyxHQUFXLENBQUMsQ0FBQztRQUN4QixjQUFRLEdBQVcsSUFBSSxDQUFDO1FBQ3hCLG9CQUFjLEdBQW1CLElBQUksQ0FBQztRQUN2QyxxQkFBZSxHQUFZLEtBQUssQ0FBQztRQUd4QyxrQkFBWSxHQUFjLElBQUksQ0FBQztRQUcvQixrQkFBWSxHQUFZLElBQUksQ0FBQztRQUc3QixlQUFTLEdBQVksSUFBSSxDQUFDO1FBRzFCLG9CQUFjLEdBQXFCLEVBQUUsQ0FBQztRQUd0QyxZQUFNLEdBQWMsSUFBSSxDQUFDO1FBR3pCLGVBQVMsR0FBYyxJQUFJLENBQUM7UUFHNUIsZUFBUyxHQUFpQixJQUFJLENBQUM7O1FBaUQvQixpQkFBaUI7SUFDckIsQ0FBQztJQWhERyx1QkFBTSxHQUFOLGNBQVcsQ0FBQztJQUVaLHFCQUFJLEdBQUosVUFBTSxVQUEwQixFQUFFLE1BQWM7UUFDNUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxVQUFVLENBQUM7UUFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUM7UUFDdkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzlFLENBQUM7SUFFRCxzQkFBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDMUUsQ0FBQztJQUVELCtCQUFjLEdBQWQ7UUFDSSxJQUFJLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxFQUFFO1lBQ3RCLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDakQsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ25CLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQ3pFO2FBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDOUIsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNqRCxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7WUFDcEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7WUFDNUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQ3ZCLEVBQUUsQ0FBQyxRQUFRLENBQ1AsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUNyQixFQUFFLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQ3ZDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFDckIsRUFBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQ3ZELENBQ0osQ0FBQTtTQUNKO2FBQU07WUFDSCxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3JFO0lBQ0wsQ0FBQztJQUVELDJCQUFVLEdBQVY7UUFDSSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUNsQyxDQUFDO0lBRUQsd0NBQXVCLEdBQXZCO1FBQ0ksS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN4QixJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN6QyxNQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDNUUsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDOUI7SUFDTCxDQUFDO0lBakVEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUM7Z0RBQ1c7SUFHL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztnREFDVztJQUc3QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzZDQUNRO0lBRzFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7a0RBQ2E7SUFHdEM7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzswQ0FDSztJQUd6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDOzZDQUNRO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUM7NkNBQ1E7SUExQmQsTUFBTTtRQUQxQixPQUFPO09BQ2EsTUFBTSxDQTRFMUI7SUFBRCxhQUFDO0NBNUVELEFBNEVDLENBNUVtQyxFQUFFLENBQUMsU0FBUyxHQTRFL0M7a0JBNUVvQixNQUFNIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGdhbWVDb250cm9sbGVyIGZyb20gXCIuL2dhbWVDb250cm9sbGVyXCI7XG5cbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xuXG5AY2NjbGFzc1xuZXhwb3J0IGRlZmF1bHQgY2xhc3MgcG9wcGVyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblxuICAgIHByaXZhdGUgcG9wcGVyTGV2ZWw6IG51bWJlciA9IDI7XG4gICAgcHJpdmF0ZSBwb3BwZXJJRDogbnVtYmVyID0gbnVsbDtcbiAgICBwcml2YXRlIGdhbWVDb250cm9sbGVyOiBnYW1lQ29udHJvbGxlciA9IG51bGw7XG4gICAgcHVibGljIHBvcHBlckRlc3Ryb3llZDogYm9vbGVhbiA9IGZhbHNlO1xuXG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZSlcbiAgICBwb3BwZXJTcHJpdGU6IGNjLlNwcml0ZSA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcbiAgICBwb3BwZXJFeHBsb2Q6IGNjLk5vZGUgPSBudWxsO1xuXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXG4gICAgcG9wcGVyRXllOiBjYy5Ob2RlID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5TcHJpdGVGcmFtZSlcbiAgICBwb3BwZXJWYXJpZW50czogY2MuU3ByaXRlRnJhbWVbXSA9IFtdO1xuXG4gICAgQHByb3BlcnR5KGNjLlByZWZhYilcbiAgICByb2NrZXQ6IGNjLlByZWZhYiA9IG51bGw7XG5cbiAgICBAcHJvcGVydHkoY2MuQnV0dG9uKVxuICAgIHBvcHBlckJ0bjogY2MuQnV0dG9uID0gbnVsbDtcblxuICAgIEBwcm9wZXJ0eShjYy5BdWRpb0NsaXApXG4gICAgcG9wcGVyVGFwOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xuXG4gICAgb25Mb2FkICgpIHt9XG5cbiAgICBpbml0IChjb250cm9sbGVyOiBnYW1lQ29udHJvbGxlciwgaXRlbUlEOiBudW1iZXIpIHtcbiAgICAgICAgdGhpcy5nYW1lQ29udHJvbGxlciA9IGNvbnRyb2xsZXI7XG4gICAgICAgIHRoaXMucG9wcGVySUQgPSBpdGVtSUQ7XG4gICAgICAgIHRoaXMucG9wcGVyTGV2ZWwgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiB0aGlzLnBvcHBlclZhcmllbnRzLmxlbmd0aCk7XG4gICAgfVxuXG4gICAgc3RhcnQgKCkge1xuICAgICAgICB0aGlzLnBvcHBlclNwcml0ZS5zcHJpdGVGcmFtZSA9IHRoaXMucG9wcGVyVmFyaWVudHNbdGhpcy5wb3BwZXJMZXZlbF07XG4gICAgfVxuXG4gICAgb25Qb3BwZXJUYXBwZWQoKSB7XG4gICAgICAgIGlmICh0aGlzLnBvcHBlckxldmVsID4gMCkge1xuICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdCh0aGlzLnBvcHBlclRhcCwgZmFsc2UpO1xuICAgICAgICAgICAgdGhpcy5wb3BwZXJMZXZlbC0tO1xuICAgICAgICAgICAgdGhpcy5wb3BwZXJTcHJpdGUuc3ByaXRlRnJhbWUgPSB0aGlzLnBvcHBlclZhcmllbnRzW3RoaXMucG9wcGVyTGV2ZWxdO1xuICAgICAgICB9IGVsc2UgaWYgKCF0aGlzLnBvcHBlckRlc3Ryb3llZCkge1xuICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdCh0aGlzLnBvcHBlclRhcCwgZmFsc2UpO1xuICAgICAgICAgICAgdGhpcy5wb3BwZXJCdG4uaW50ZXJhY3RhYmxlID0gZmFsc2U7XG4gICAgICAgICAgICB0aGlzLnBvcHBlckRlc3Ryb3llZCA9IHRydWU7XG4gICAgICAgICAgICB0aGlzLnBvcHBlckV4cGxvZC5ydW5BY3Rpb24oXG4gICAgICAgICAgICAgICAgY2Muc2VxdWVuY2UoXG4gICAgICAgICAgICAgICAgICAgIGNjLnNjYWxlVG8oMC41LCAxLCAxKSxcbiAgICAgICAgICAgICAgICAgICAgY2MuY2FsbEZ1bmModGhpcy5oaWRlUG9wcGVyLmJpbmQodGhpcykpLFxuICAgICAgICAgICAgICAgICAgICBjYy5zY2FsZVRvKDAuNSwgMCwgMCksXG4gICAgICAgICAgICAgICAgICAgIGNjLmNhbGxGdW5jKHRoaXMub25Qb3BwZXJEZXN0cm95Q2FsbGJhY2suYmluZCh0aGlzKSlcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmdhbWVDb250cm9sbGVyLmdyaWRJdGVtTWFuYWdlci5yZW1vdmVHcmlkSXRlbSh0aGlzLnBvcHBlcklEKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGhpZGVQb3BwZXIoKSB7XG4gICAgICAgIHRoaXMucG9wcGVyU3ByaXRlLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIHRoaXMucG9wcGVyRXllLmFjdGl2ZSA9IGZhbHNlO1xuICAgIH1cblxuICAgIG9uUG9wcGVyRGVzdHJveUNhbGxiYWNrKCkge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IDQ7IGkrKykge1xuICAgICAgICAgICAgbGV0IHJvY2tldCA9IGNjLmluc3RhbnRpYXRlKHRoaXMucm9ja2V0KTtcbiAgICAgICAgICAgIHJvY2tldC5nZXRDb21wb25lbnQoXCJjb2xsaWRlclwiKS5pbml0KGksIHRoaXMucG9wcGVySUQsIHRoaXMuZ2FtZUNvbnRyb2xsZXIpO1xuICAgICAgICAgICAgdGhpcy5ub2RlLmFkZENoaWxkKHJvY2tldCk7XG4gICAgICAgIH0gIFxuICAgIH1cblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XG59XG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/collider.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '21deazBcsNDKZuMcwda1EIx', 'collider');
// scripts/collider.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var collider = /** @class */ (function (_super) {
    __extends(collider, _super);
    function collider() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.popperID = null;
        _this.colliderDirection = null;
        _this.rocketMove = null;
        _this.rocketTriggered = false;
        _this.collider = null;
        return _this;
        // update (dt) {}
    }
    // onLoad () {}
    collider.prototype.start = function () {
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
    };
    collider.prototype.init = function (direction, popperID, controller) {
        var _this = this;
        this.colliderDirection = direction;
        this.popperID = popperID;
        this.gameController = controller;
        var popperChild = this.gameController.gridItemManager.grid[this.popperID].gridCurrItem.getChildByName("popper");
        if (direction == 0) {
            var maxDistance_1 = this.node.x + this.gameController.gridArea.width * 2;
            this.rocketMove = window.setInterval(function () {
                _this.node.x += 3;
                if (_this.node.x > maxDistance_1) {
                    window.clearInterval(_this.rocketMove);
                    if (popperChild) {
                        popperChild.getComponent("popper").onPopperTapped();
                    }
                    else {
                        console.log("Error...");
                    }
                    _this.destroy();
                }
            }, 10);
        }
        else if (direction == 1) {
            var maxDistance_2 = this.node.y + this.gameController.gridArea.height * 2;
            this.rocketMove = window.setInterval(function () {
                _this.node.y += 3;
                if (_this.node.y > maxDistance_2) {
                    window.clearInterval(_this.rocketMove);
                    if (popperChild) {
                        popperChild.getComponent("popper").onPopperTapped();
                    }
                    else {
                        console.log("Error...");
                    }
                    _this.destroy();
                }
            }, 10);
        }
        else if (direction == 2) {
            var maxDistance_3 = this.node.x - this.gameController.gridArea.width * 2;
            this.rocketMove = window.setInterval(function () {
                _this.node.x -= 3;
                if (_this.node.x < maxDistance_3) {
                    window.clearInterval(_this.rocketMove);
                    if (popperChild) {
                        popperChild.getComponent("popper").onPopperTapped();
                    }
                    else {
                        console.log("Error...");
                    }
                    _this.destroy();
                }
            }, 10);
        }
        else {
            var maxDistance_4 = this.node.y - this.gameController.gridArea.height * 2;
            this.rocketMove = window.setInterval(function () {
                _this.node.y -= 3;
                if (_this.node.y < maxDistance_4) {
                    window.clearInterval(_this.rocketMove);
                    if (popperChild) {
                        popperChild.getComponent("popper").onPopperTapped();
                    }
                    else {
                        console.log("Error...");
                    }
                    _this.destroy();
                }
            }, 10);
        }
    };
    collider.prototype.onCollisionEnter = function (other, self) {
        if (this.rocketTriggered && other.node.getComponent("popper") && !other.node.getComponent("popper").popperDestroyed) {
            other.node.getComponent("popper").onPopperTapped();
            window.clearInterval(this.rocketMove);
            self.node.destroy();
            // this.gameController.gridItemManager.removeGridItem(this.popperID);
        }
    };
    collider.prototype.onCollisionStay = function (other, self) {
    };
    collider.prototype.onCollisionExit = function (other, self) {
        this.rocketTriggered = true;
    };
    __decorate([
        property(cc.BoxCollider)
    ], collider.prototype, "collider", void 0);
    collider = __decorate([
        ccclass
    ], collider);
    return collider;
}(cc.Component));
exports.default = collider;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL2NvbGxpZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVNLElBQUEsS0FBc0IsRUFBRSxDQUFDLFVBQVUsRUFBbEMsT0FBTyxhQUFBLEVBQUUsUUFBUSxjQUFpQixDQUFDO0FBRzFDO0lBQXNDLDRCQUFZO0lBQWxEO1FBQUEscUVBbUdDO1FBaEdXLGNBQVEsR0FBVyxJQUFJLENBQUM7UUFDeEIsdUJBQWlCLEdBQVcsSUFBSSxDQUFDO1FBQ2pDLGdCQUFVLEdBQVcsSUFBSSxDQUFDO1FBQzFCLHFCQUFlLEdBQVksS0FBSyxDQUFDO1FBR3pDLGNBQVEsR0FBbUIsSUFBSSxDQUFDOztRQXlGaEMsaUJBQWlCO0lBQ3JCLENBQUM7SUF4RkcsZUFBZTtJQUVmLHdCQUFLLEdBQUw7UUFDSSxJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDaEQsT0FBTyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7SUFDM0IsQ0FBQztJQUVELHVCQUFJLEdBQUosVUFBSyxTQUFpQixFQUFFLFFBQWdCLEVBQUUsVUFBMEI7UUFBcEUsaUJBOERDO1FBN0RHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxTQUFTLENBQUM7UUFDbkMsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDekIsSUFBSSxDQUFDLGNBQWMsR0FBRyxVQUFVLENBQUM7UUFDakMsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2hILElBQUksU0FBUyxJQUFJLENBQUMsRUFBRTtZQUNoQixJQUFJLGFBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFDO1lBQ3JFLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztnQkFDakMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNqQixJQUFJLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLGFBQVcsRUFBRTtvQkFDM0IsTUFBTSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ3RDLElBQUksV0FBVyxFQUFFO3dCQUNiLFdBQVcsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7cUJBQ3ZEO3lCQUFNO3dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7cUJBQzNCO29CQUNELEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztpQkFDbEI7WUFDTCxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7U0FDVjthQUFNLElBQUksU0FBUyxJQUFJLENBQUMsRUFBRTtZQUN2QixJQUFJLGFBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUMsQ0FBQyxDQUFDO1lBQ3RFLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztnQkFDakMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNqQixJQUFJLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLGFBQVcsRUFBRTtvQkFDM0IsTUFBTSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ3RDLElBQUksV0FBVyxFQUFFO3dCQUNiLFdBQVcsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7cUJBQ3ZEO3lCQUFNO3dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7cUJBQzNCO29CQUNELEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztpQkFDbEI7WUFDTCxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7U0FDVjthQUFNLElBQUksU0FBUyxJQUFJLENBQUMsRUFBRTtZQUN2QixJQUFJLGFBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUMsQ0FBQyxDQUFDO1lBQ3JFLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztnQkFDakMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNqQixJQUFJLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLGFBQVcsRUFBRTtvQkFDM0IsTUFBTSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ3RDLElBQUksV0FBVyxFQUFFO3dCQUNiLFdBQVcsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7cUJBQ3ZEO3lCQUFNO3dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7cUJBQzNCO29CQUNELEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztpQkFDbEI7WUFDTCxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7U0FDVjthQUFNO1lBQ0gsSUFBSSxhQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFDLENBQUMsQ0FBQztZQUN0RSxJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUM7Z0JBQ2pDLEtBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDakIsSUFBSSxLQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxhQUFXLEVBQUU7b0JBQzNCLE1BQU0sQ0FBQyxhQUFhLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUN0QyxJQUFJLFdBQVcsRUFBRTt3QkFDYixXQUFXLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO3FCQUN2RDt5QkFBTTt3QkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO3FCQUMzQjtvQkFDRCxLQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7aUJBQ2xCO1lBQ0wsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1NBQ1Y7SUFDTCxDQUFDO0lBRUQsbUNBQWdCLEdBQWhCLFVBQWtCLEtBQVUsRUFBRSxJQUFTO1FBQ25DLElBQUksSUFBSSxDQUFDLGVBQWUsSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGVBQWUsRUFBRTtZQUNqSCxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUNuRCxNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3BCLHFFQUFxRTtTQUN4RTtJQUNMLENBQUM7SUFFRCxrQ0FBZSxHQUFmLFVBQWlCLEtBQVUsRUFBRSxJQUFTO0lBQ3RDLENBQUM7SUFFRCxrQ0FBZSxHQUFmLFVBQWlCLEtBQVUsRUFBRSxJQUFTO1FBQ2xDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO0lBQ2hDLENBQUM7SUF2RkQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQzs4Q0FDTztJQVRmLFFBQVE7UUFENUIsT0FBTztPQUNhLFFBQVEsQ0FtRzVCO0lBQUQsZUFBQztDQW5HRCxBQW1HQyxDQW5HcUMsRUFBRSxDQUFDLFNBQVMsR0FtR2pEO2tCQW5Hb0IsUUFBUSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBnYW1lQ29udHJvbGxlciBmcm9tIFwiLi9nYW1lQ29udHJvbGxlclwiO1xuXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcblxuQGNjY2xhc3NcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIGNvbGxpZGVyIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcblxuICAgIHByaXZhdGUgZ2FtZUNvbnRyb2xsZXI6IGdhbWVDb250cm9sbGVyO1xuICAgIHByaXZhdGUgcG9wcGVySUQ6IG51bWJlciA9IG51bGw7XG4gICAgcHJpdmF0ZSBjb2xsaWRlckRpcmVjdGlvbjogbnVtYmVyID0gbnVsbDtcbiAgICBwcml2YXRlIHJvY2tldE1vdmU6IG51bWJlciA9IG51bGw7XG4gICAgcHJpdmF0ZSByb2NrZXRUcmlnZ2VyZWQ6IGJvb2xlYW4gPSBmYWxzZTtcblxuICAgIEBwcm9wZXJ0eShjYy5Cb3hDb2xsaWRlcilcbiAgICBjb2xsaWRlcjogY2MuQm94Q29sbGlkZXIgPSBudWxsO1xuXG4gICAgLy8gb25Mb2FkICgpIHt9XG5cbiAgICBzdGFydCAoKSB7XG4gICAgICAgIHZhciBtYW5hZ2VyID0gY2MuZGlyZWN0b3IuZ2V0Q29sbGlzaW9uTWFuYWdlcigpO1xuICAgICAgICBtYW5hZ2VyLmVuYWJsZWQgPSB0cnVlO1xuICAgIH1cblxuICAgIGluaXQoZGlyZWN0aW9uOiBudW1iZXIsIHBvcHBlcklEOiBudW1iZXIsIGNvbnRyb2xsZXI6IGdhbWVDb250cm9sbGVyKSB7XG4gICAgICAgIHRoaXMuY29sbGlkZXJEaXJlY3Rpb24gPSBkaXJlY3Rpb247XG4gICAgICAgIHRoaXMucG9wcGVySUQgPSBwb3BwZXJJRDtcbiAgICAgICAgdGhpcy5nYW1lQ29udHJvbGxlciA9IGNvbnRyb2xsZXI7XG4gICAgICAgIGxldCBwb3BwZXJDaGlsZCA9IHRoaXMuZ2FtZUNvbnRyb2xsZXIuZ3JpZEl0ZW1NYW5hZ2VyLmdyaWRbdGhpcy5wb3BwZXJJRF0uZ3JpZEN1cnJJdGVtLmdldENoaWxkQnlOYW1lKFwicG9wcGVyXCIpO1xuICAgICAgICBpZiAoZGlyZWN0aW9uID09IDApIHtcbiAgICAgICAgICAgIGxldCBtYXhEaXN0YW5jZSA9IHRoaXMubm9kZS54ICsgdGhpcy5nYW1lQ29udHJvbGxlci5ncmlkQXJlYS53aWR0aCoyO1xuICAgICAgICAgICAgdGhpcy5yb2NrZXRNb3ZlID0gd2luZG93LnNldEludGVydmFsKCgpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueCArPSAzO1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLm5vZGUueCA+IG1heERpc3RhbmNlKSB7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5jbGVhckludGVydmFsKHRoaXMucm9ja2V0TW92ZSk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChwb3BwZXJDaGlsZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcG9wcGVyQ2hpbGQuZ2V0Q29tcG9uZW50KFwicG9wcGVyXCIpLm9uUG9wcGVyVGFwcGVkKCk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkVycm9yLi4uXCIpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGVzdHJveSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sIDEwKTtcbiAgICAgICAgfSBlbHNlIGlmIChkaXJlY3Rpb24gPT0gMSkge1xuICAgICAgICAgICAgbGV0IG1heERpc3RhbmNlID0gdGhpcy5ub2RlLnkgKyB0aGlzLmdhbWVDb250cm9sbGVyLmdyaWRBcmVhLmhlaWdodCoyO1xuICAgICAgICAgICAgdGhpcy5yb2NrZXRNb3ZlID0gd2luZG93LnNldEludGVydmFsKCgpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueSArPSAzO1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLm5vZGUueSA+IG1heERpc3RhbmNlKSB7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5jbGVhckludGVydmFsKHRoaXMucm9ja2V0TW92ZSk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChwb3BwZXJDaGlsZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcG9wcGVyQ2hpbGQuZ2V0Q29tcG9uZW50KFwicG9wcGVyXCIpLm9uUG9wcGVyVGFwcGVkKCk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkVycm9yLi4uXCIpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGVzdHJveSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sIDEwKTtcbiAgICAgICAgfSBlbHNlIGlmIChkaXJlY3Rpb24gPT0gMikge1xuICAgICAgICAgICAgbGV0IG1heERpc3RhbmNlID0gdGhpcy5ub2RlLnggLSB0aGlzLmdhbWVDb250cm9sbGVyLmdyaWRBcmVhLndpZHRoKjI7XG4gICAgICAgICAgICB0aGlzLnJvY2tldE1vdmUgPSB3aW5kb3cuc2V0SW50ZXJ2YWwoKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMubm9kZS54IC09IDM7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMubm9kZS54IDwgbWF4RGlzdGFuY2UpIHtcbiAgICAgICAgICAgICAgICAgICAgd2luZG93LmNsZWFySW50ZXJ2YWwodGhpcy5yb2NrZXRNb3ZlKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHBvcHBlckNoaWxkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwb3BwZXJDaGlsZC5nZXRDb21wb25lbnQoXCJwb3BwZXJcIikub25Qb3BwZXJUYXBwZWQoKTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXJyb3IuLi5cIik7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kZXN0cm95KCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSwgMTApO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgbGV0IG1heERpc3RhbmNlID0gdGhpcy5ub2RlLnkgLSB0aGlzLmdhbWVDb250cm9sbGVyLmdyaWRBcmVhLmhlaWdodCoyO1xuICAgICAgICAgICAgdGhpcy5yb2NrZXRNb3ZlID0gd2luZG93LnNldEludGVydmFsKCgpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueSAtPSAzO1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLm5vZGUueSA8IG1heERpc3RhbmNlKSB7XG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5jbGVhckludGVydmFsKHRoaXMucm9ja2V0TW92ZSk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChwb3BwZXJDaGlsZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcG9wcGVyQ2hpbGQuZ2V0Q29tcG9uZW50KFwicG9wcGVyXCIpLm9uUG9wcGVyVGFwcGVkKCk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkVycm9yLi4uXCIpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGVzdHJveSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sIDEwKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG9uQ29sbGlzaW9uRW50ZXIgKG90aGVyOiBhbnksIHNlbGY6IGFueSkge1xuICAgICAgICBpZiAodGhpcy5yb2NrZXRUcmlnZ2VyZWQgJiYgb3RoZXIubm9kZS5nZXRDb21wb25lbnQoXCJwb3BwZXJcIikgJiYgIW90aGVyLm5vZGUuZ2V0Q29tcG9uZW50KFwicG9wcGVyXCIpLnBvcHBlckRlc3Ryb3llZCkge1xuICAgICAgICAgICAgb3RoZXIubm9kZS5nZXRDb21wb25lbnQoXCJwb3BwZXJcIikub25Qb3BwZXJUYXBwZWQoKTtcbiAgICAgICAgICAgIHdpbmRvdy5jbGVhckludGVydmFsKHRoaXMucm9ja2V0TW92ZSk7XG4gICAgICAgICAgICBzZWxmLm5vZGUuZGVzdHJveSgpO1xuICAgICAgICAgICAgLy8gdGhpcy5nYW1lQ29udHJvbGxlci5ncmlkSXRlbU1hbmFnZXIucmVtb3ZlR3JpZEl0ZW0odGhpcy5wb3BwZXJJRCk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgXG4gICAgb25Db2xsaXNpb25TdGF5IChvdGhlcjogYW55LCBzZWxmOiBhbnkpIHtcbiAgICB9XG4gICAgXG4gICAgb25Db2xsaXNpb25FeGl0IChvdGhlcjogYW55LCBzZWxmOiBhbnkpIHtcbiAgICAgICAgdGhpcy5yb2NrZXRUcmlnZ2VyZWQgPSB0cnVlO1xuICAgIH1cblxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XG59XG4iXX0=
//------QC-SOURCE-SPLIT------
