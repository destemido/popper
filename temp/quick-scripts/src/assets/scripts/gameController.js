"use strict";
cc._RF.push(module, 'bce8c4j/pRE16TGUHmYYw3D', 'gameController');
// scripts/gameController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var gridItemManager_1 = require("./gridItemManager");
//app password : FAAprgF2rs2WKjH4j54p
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var gameController = /** @class */ (function (_super) {
    __extends(gameController, _super);
    function gameController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.rowSize = 5;
        _this.columnSize = 6;
        _this.gridItem = null;
        _this.gridArea = null;
        _this.gridItemManager = null;
        _this.levelUp = null;
        _this.gameOver = null;
        _this.level = null;
        _this.gridPos = [];
        _this.userLevel = 1;
        return _this;
        // update (dt) {}
    }
    gameController.prototype.onLoad = function () {
        this.createGrid();
    };
    gameController.prototype.start = function () {
        this.level.string = this.userLevel.toString();
        this.gridItemManager.initializeGamePlay(this);
    };
    gameController.prototype.createGrid = function () {
        this.gridArea.width = this.gridItem.width * this.columnSize;
        this.gridArea.height = this.gridItem.height * this.rowSize;
        var currItemPos = this.gridItem.getPosition();
        for (var i = 0; i < this.rowSize; i++) {
            for (var j = 0; j < this.columnSize; j++) {
                var itemPos = this.gridItem.getPosition();
                itemPos.x = currItemPos.x + j * this.gridItem.width;
                itemPos.y = currItemPos.y - i * this.gridItem.height;
                this.gridPos.push(itemPos);
            }
        }
    };
    gameController.prototype.getGridPos = function () {
        return this.gridPos;
    };
    gameController.prototype.getGridDimention = function () {
        return {
            row: this.rowSize,
            column: this.columnSize
        };
    };
    gameController.prototype.showLevelUp = function () {
        this.levelUp.runAction(cc.scaleTo(0.5, 1, 1));
    };
    gameController.prototype.showGameOver = function () {
        // this.gameOver.runAction(
        //     cc.sequence(
        //         cc.scaleTo(0.5, 1, 1),
        //         cc.scaleTo(0.5, 0, 0),
        //         cc.callFunc(() => {
        //             cc.director.loadScene("game");
        //         })
        //     )
        // );
        this.gameOver.runAction(cc.scaleTo(0.5, 1, 1));
    };
    gameController.prototype.onNextLevelClick = function () {
        this.levelUp.runAction(cc.sequence(cc.scaleTo(0.5, 0, 0), cc.callFunc(this.loadNextLevel.bind(this))));
    };
    gameController.prototype.loadNextLevel = function () {
        this.userLevel++;
        this.level.string = this.userLevel.toString();
        this.gridItemManager.createGrid(this.userLevel);
    };
    __decorate([
        property
    ], gameController.prototype, "rowSize", void 0);
    __decorate([
        property
    ], gameController.prototype, "columnSize", void 0);
    __decorate([
        property(cc.Node)
    ], gameController.prototype, "gridItem", void 0);
    __decorate([
        property(cc.Node)
    ], gameController.prototype, "gridArea", void 0);
    __decorate([
        property(gridItemManager_1.default)
    ], gameController.prototype, "gridItemManager", void 0);
    __decorate([
        property(cc.Node)
    ], gameController.prototype, "levelUp", void 0);
    __decorate([
        property(cc.Node)
    ], gameController.prototype, "gameOver", void 0);
    __decorate([
        property(cc.Label)
    ], gameController.prototype, "level", void 0);
    gameController = __decorate([
        ccclass
    ], gameController);
    return gameController;
}(cc.Component));
exports.default = gameController;

cc._RF.pop();