"use strict";
cc._RF.push(module, '997ecnFPMNNBaOcj5hdKsKF', 'popper');
// scripts/popper.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var popper = /** @class */ (function (_super) {
    __extends(popper, _super);
    function popper() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.popperLevel = 2;
        _this.popperID = null;
        _this.gameController = null;
        _this.popperDestroyed = false;
        _this.popperSprite = null;
        _this.popperExplod = null;
        _this.popperEye = null;
        _this.popperVarients = [];
        _this.rocket = null;
        _this.popperBtn = null;
        _this.popperTap = null;
        return _this;
        // update (dt) {}
    }
    popper.prototype.onLoad = function () { };
    popper.prototype.init = function (controller, itemID) {
        this.gameController = controller;
        this.popperID = itemID;
        this.popperLevel = Math.floor(Math.random() * this.popperVarients.length);
    };
    popper.prototype.start = function () {
        this.popperSprite.spriteFrame = this.popperVarients[this.popperLevel];
    };
    popper.prototype.onPopperTapped = function () {
        if (this.popperLevel > 0) {
            cc.audioEngine.playEffect(this.popperTap, false);
            this.popperLevel--;
            this.popperSprite.spriteFrame = this.popperVarients[this.popperLevel];
        }
        else if (!this.popperDestroyed) {
            cc.audioEngine.playEffect(this.popperTap, false);
            this.popperBtn.interactable = false;
            this.popperDestroyed = true;
            this.popperExplod.runAction(cc.sequence(cc.scaleTo(0.5, 1, 1), cc.callFunc(this.hidePopper.bind(this)), cc.scaleTo(0.5, 0, 0), cc.callFunc(this.onPopperDestroyCallback.bind(this))));
        }
        else {
            this.gameController.gridItemManager.removeGridItem(this.popperID);
        }
    };
    popper.prototype.hidePopper = function () {
        this.popperSprite.node.active = false;
        this.popperEye.active = false;
    };
    popper.prototype.onPopperDestroyCallback = function () {
        for (var i = 0; i < 4; i++) {
            var rocket = cc.instantiate(this.rocket);
            rocket.getComponent("collider").init(i, this.popperID, this.gameController);
            this.node.addChild(rocket);
        }
    };
    __decorate([
        property(cc.Sprite)
    ], popper.prototype, "popperSprite", void 0);
    __decorate([
        property(cc.Node)
    ], popper.prototype, "popperExplod", void 0);
    __decorate([
        property(cc.Node)
    ], popper.prototype, "popperEye", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], popper.prototype, "popperVarients", void 0);
    __decorate([
        property(cc.Prefab)
    ], popper.prototype, "rocket", void 0);
    __decorate([
        property(cc.Button)
    ], popper.prototype, "popperBtn", void 0);
    __decorate([
        property(cc.AudioClip)
    ], popper.prototype, "popperTap", void 0);
    popper = __decorate([
        ccclass
    ], popper);
    return popper;
}(cc.Component));
exports.default = popper;

cc._RF.pop();